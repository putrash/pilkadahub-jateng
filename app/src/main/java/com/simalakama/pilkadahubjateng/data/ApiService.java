package com.simalakama.pilkadahubjateng.data;

import com.simalakama.pilkadahubjateng.model.Response.ModelAgenda;
import com.simalakama.pilkadahubjateng.model.Response.ModelBerita;
import com.simalakama.pilkadahubjateng.model.Response.ModelDaerah;
import com.simalakama.pilkadahubjateng.model.Response.ModelDps;
import com.simalakama.pilkadahubjateng.model.Response.ModelJawaban;
import com.simalakama.pilkadahubjateng.model.Response.ModelLaporan;
import com.simalakama.pilkadahubjateng.model.Response.ModelPaslon;
import com.simalakama.pilkadahubjateng.model.Response.ModelPertanyaan;
import com.simalakama.pilkadahubjateng.model.Response.ModelPpdb;
import com.simalakama.pilkadahubjateng.model.Response.ModelProker;
import com.simalakama.pilkadahubjateng.model.Response.ModelVideo;

import java.util.List;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by Putraa on 4/19/2018.
 */

public interface ApiService {
    String baseUrl = "https://pilkada-hub.herokuapp.com/api/";

    @POST("complaint")
    Call<ModelLaporan> postComplaint(@Body ModelLaporan modelLaporan);

    @POST("question")
    Call<ModelLaporan> tanya(@Body ModelPertanyaan modelPertanyaan);

    @GET("filter")
    Call<List<ModelDaerah>> getDaerah();

    @GET("dps/cari/{nik}/{nama}")
    Call<List<ModelDps>> getDps(@Path("nik") String nik, @Path("nama") String nama);

    @GET("ppdp/cari/{kabupaten}/{kecamatan}/{kelurahan}")
    Call<List<ModelPpdb>> getPpdb(@Path("kabupaten") String kabupaten, @Path("kecamatan") String kecamatan, @Path("kelurahan") String kelurahan);

    @GET("agenda")
    Call<List<ModelAgenda>> getAgenda();

    @GET("question")
    Call<List<ModelPertanyaan>> getPertanyaan();

    @GET("jawaban")
    Call<List<ModelJawaban>> getJawaban();

    @GET("news")
    Call<List<ModelBerita>> getBerita();

    @GET("video")
    Call<List<ModelVideo>> getVideo();

    @GET("news/popular")
    Call<List<ModelBerita>> getBeritaPopuler();

    @GET("paslon/{id}")
    Call<ModelPaslon> getDetailPaslon(@Path("id") String id);

    @GET("proker/{id}")
    Call<ModelProker> getProkerPaslon(@Path("id") String id);

    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .build();
    ApiService service = retrofit.create(ApiService.class);
}
