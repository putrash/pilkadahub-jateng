package com.simalakama.pilkadahubjateng;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.simalakama.pilkadahubjateng.custom.CustomDialog;
import com.simalakama.pilkadahubjateng.data.ApiService;
import com.simalakama.pilkadahubjateng.model.Response.ModelLaporan;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PengaduanActivity extends AppCompatActivity {

    private Spinner spinnerPengaduan;

    public DatabaseReference mRootReference = FirebaseDatabase.getInstance().getReference();
    private StorageReference storageRef = FirebaseStorage.getInstance().getReference();

    private Bitmap bitmap;
    private File destination = null;
    private InputStream inputStreamImg;
    private String imgPath;

    private final int PICK_IMAGE_CAMERA = 1, PICK_IMAGE_GALLERY = 2;

    private ImageView imageView;
    private Button btnBukti, btnKirim;
    private EditText textNama, textAlamat, textHp, textPengaduan, textEmail;
    private RelativeLayout progressbar;
    private String tipe_laporan = "";

    boolean progess1, progress2;
    byte[] imgByte;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pengaduan);

        // Set a Toolbar to replace the ActionBar and add Padding.
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setPadding(0, getToolBarHeight(), 0, 0);

        // Init UI
        btnBukti = findViewById(R.id.btn_bukti);
        btnKirim = findViewById(R.id.btn_kirimpengaduan);
        imageView = findViewById(R.id.image_view);
        textNama = findViewById(R.id.text_nama);
        textAlamat = findViewById(R.id.text_alamat);
        textHp = findViewById(R.id.text_nomorhp);
        textPengaduan = findViewById(R.id.text_isi);
        textEmail = findViewById(R.id.text_email);
        spinnerPengaduan = findViewById(R.id.spinner_pengaduan);
        progressbar = findViewById(R.id.progressbar);

        //Instance
        String nama = null,email = null,alamat = null,no_hp = null,isi_pengaduan = null;
        if (savedInstanceState != null){
            nama = savedInstanceState.getString("nama");
            email = savedInstanceState.getString("email");
            alamat = savedInstanceState.getString("alamat");
            no_hp = savedInstanceState.getString("no_hp");
            isi_pengaduan = savedInstanceState.getString("isi_pengaduan");

        }

        if (nama != null)
            textNama.setText(nama);
        if (email != null)
            textEmail.setText(email);
        if (alamat != null)
            textAlamat.setText(alamat);
        if (no_hp != null)
            textHp.setText(no_hp);
        if (isi_pengaduan != null)
            textPengaduan.setText(isi_pengaduan);


        //modal listener
        imageView.setOnClickListener(v -> {
            Dialog modal = new Dialog(PengaduanActivity.this);
            modal.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            modal.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            LayoutInflater inflater = getLayoutInflater();
            View newView = inflater.inflate(R.layout.image_modal, null);
            modal.setContentView(newView);
            ImageView iv = newView.findViewById(R.id.img);
            LinearLayout ll = newView.findViewById(R.id.layout);
            if (bitmap == null) {
                iv.setImageDrawable(this.getResources().getDrawable(R.drawable.icon_frame));
            } else {
                iv.setImageBitmap(bitmap);
            }
            iv.setOnClickListener(v1 -> modal.dismiss());
            ll.setOnClickListener(v1 -> modal.dismiss());
            modal.show();
        });

        //Initiate Button Kirim Pengaduan
        btnBukti.setOnClickListener(v -> selectImage());
        btnKirim.setOnClickListener(v -> {
            if (textNama.getText().toString().matches("") || textEmail.getText().toString().matches("") ||
                    textAlamat.getText().toString().matches("") || textHp.getText().toString().matches("") ||
                    textPengaduan.getText().toString().matches("") || tipe_laporan.matches("Laporan")) {
                showMessageBox("Mohon data laporan dilengkapi");
            } else {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                alertDialogBuilder.setTitle("PilkadaHub");
                alertDialogBuilder
                        .setMessage("Apakah anda yakin?")
                        .setCancelable(false)
                        .setPositiveButton("Ya", (dialog, id) -> {
                            uploadData();
                        })
                        .setNegativeButton("Tidak", (dialog, id) -> dialog.cancel());
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        });

        dataPengaduan();
    }

    private File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        String imageFileName = "IMG_" + timeStamp;
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        destination = File.createTempFile(
                imageFileName,  /* prefix */
                ".JPG",         /* suffix */
                storageDir      /* directory */
        );
        imgPath = destination.getAbsolutePath();
        return destination;
    }

    private void selectImage() {
        try {
            int hasPermOne = ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA);
            int hasPermTwo = ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
            int hasPermThree = ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE);
            int perm = PackageManager.PERMISSION_GRANTED;
            if (hasPermOne == perm && hasPermTwo == perm && hasPermThree == perm) {
                final CharSequence[] options = {"Ambil Foto", "Pilih Dari Galeri", "Batal"};
                android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(PengaduanActivity.this);
                builder.setTitle("Select Option");
                builder.setItems(options, (dialog, item) -> {
                    if (options[item].equals("Ambil Foto")) {
                        dialog.dismiss();
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                        File photoFile = null;
                        try {
                            photoFile = createImageFile();
                        } catch (IOException ex) {

                        }
                        if (photoFile != null) {
                            Uri photoURI = FileProvider.getUriForFile(PengaduanActivity.this,
                                    "com.simalakama.pilkadahubjateng.fileprovider",
                                    photoFile);
                            intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                            startActivityForResult(intent, PICK_IMAGE_CAMERA);
                        }
                    } else if (options[item].equals("Pilih Dari Galeri")) {
                        dialog.dismiss();
                        Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(pickPhoto, PICK_IMAGE_GALLERY);
                    } else if (options[item].equals("Batal")) {
                        dialog.dismiss();
                    }
                });
                builder.show();
            } else if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.CAMERA)) {
            } else {
                ActivityCompat.requestPermissions(
                        this,
                        new String[]{
                                android.Manifest.permission.READ_EXTERNAL_STORAGE,
                                android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                android.Manifest.permission.CAMERA},
                        1
                );
            }
        } catch (Exception e) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    android.Manifest.permission.CAMERA)) {
            } else {
                ActivityCompat.requestPermissions(
                        this,
                        new String[]{
                                android.Manifest.permission.READ_EXTERNAL_STORAGE,
                                android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                android.Manifest.permission.CAMERA},
                        1
                );
            }

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != 0) {
            if (data != null) {
                super.onActivityResult(requestCode, resultCode, data);
                inputStreamImg = null;
                if (requestCode == PICK_IMAGE_GALLERY) {
                    Uri selectedImage = data.getData();
                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, bytes);
                        imgByte = bytes.toByteArray();
                        imgPath = getRealPathFromURI(selectedImage);
                        destination = new File(imgPath.toString());
                        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                        imageView.setImageBitmap(bitmap);
                        Uri file = Uri.fromFile(destination);
                        String foto = file.getLastPathSegment();
                        btnBukti.setText(foto);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }
            if (requestCode == PICK_IMAGE_CAMERA) {
                Uri file = Uri.fromFile(new File(imgPath));
                bitmap = BitmapFactory.decodeFile(imgPath);
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 30, bytes);
                imgByte = bytes.toByteArray();
                imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                imageView.setImageURI(file);
                String foto = file.getLastPathSegment();
                btnBukti.setText(foto);
            }
        }
    }

    public String getRealPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Audio.Media.DATA};
        Cursor cursor = managedQuery(contentUri, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    private void uploadData() {
        progressbar.setVisibility(View.VISIBLE);
        String nama, alamat, hp, tipe, laporan, email, foto, keyPush;
        nama = textNama.getText().toString();
        alamat = textAlamat.getText().toString();
        hp = textHp.getText().toString();
        tipe = tipe_laporan;
        laporan = textPengaduan.getText().toString();
        email = textEmail.getText().toString();
        foto = "";
        keyPush = "";
        progess1 = false;
        progress2 = false;

        if (destination != null) {   //kalau disertakan gambar
            Uri file = Uri.fromFile(destination);
            foto = file.getLastPathSegment();
            keyPush = mRootReference.push().getKey();
            storageRef.child("laporan/" + keyPush + "/" + foto).putBytes(imgByte).addOnFailureListener(exception -> {
                progressbar.setVisibility(View.GONE);
                showMessageBox("Proses Gagal!");
            }).addOnSuccessListener(taskSnapshot -> {
                progess1 = true;
                if (progress2) {
                    progressbar.setVisibility(View.GONE);
                    CustomDialog customDialog = new CustomDialog(PengaduanActivity.this, "Pengaduan Terkirim");
                    customDialog.show();
                }
            });
        } else {
            progess1 = true;
        }

        ModelLaporan modelLaporan = new ModelLaporan(nama, alamat, hp, tipe, laporan, new String[]{foto, keyPush}, email);
        Call<ModelLaporan> call = ApiService.service.postComplaint(modelLaporan);
        call.enqueue(new Callback<ModelLaporan>() {
            @Override
            public void onResponse(Call<ModelLaporan> call, Response<ModelLaporan> response) {
                progress2 = true;
                if (response != null && response.errorBody() != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.errorBody().toString());
                        String message = jsonObject.getString("message");
                        String errors = jsonObject.getString("errors");
                        Log.d("response", "onResponse: " + message + " error : " + errors);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                if (progess1) {
                    progressbar.setVisibility(View.GONE);
                    //Toast.makeText(PengaduanActivity.this, "Berhasil", Toast.LENGTH_SHORT).show();
                    CustomDialog customDialog = new CustomDialog(PengaduanActivity.this, "Pengaduan Terkirim");
                    customDialog.show();
                }
            }

            @Override
            public void onFailure(Call<ModelLaporan> call, Throwable t) {
                progressbar.setVisibility(View.GONE);
                Toast.makeText(PengaduanActivity.this, "Jaringan Error!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void showMessageBox(String message) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this, R.style.customAlertDialog);
        alertDialogBuilder.setTitle("PilkadaHub");
        alertDialogBuilder.setMessage(message);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton("OK", (dialogInterface, i) -> {
            dialogInterface.dismiss();
        });
        alertDialogBuilder.show();
    }

    void dataPengaduan() {
        String[] tipe = {"Tipe Pengaduan", "Kritik", "Saran", "Lapor", "Berita Hoax"};
        ArrayAdapter<String> spinnerPengaduanAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, tipe) {
            @Override
            public boolean isEnabled(int position) {
                return position != 0;
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView textView = (TextView) view;
                if (position == 0) {
                    textView.setTextColor(getResources().getColor(R.color.colorTextSecondary));
                } else {
                    textView.setTextColor(getResources().getColor(R.color.colorTextPrimary));
                }

                return view;
            }
        };
        spinnerPengaduanAdapter.setDropDownViewResource(R.layout.spinner_item);
        spinnerPengaduan.setAdapter(spinnerPengaduanAdapter);

        spinnerPengaduan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                tipe_laporan = spinnerPengaduan.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("nama",textNama.getText().toString());
        outState.putString("email",textEmail.getText().toString());
        outState.putString("alamat",textAlamat.getText().toString());
        outState.putString("no_hp",textHp.getText().toString());
        outState.putString("pengaduan",tipe_laporan);
        outState.putString("isi_pengaduan", textPengaduan.getText().toString());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private int getToolBarHeight() {
        int height;

        Resources myResources = getResources();
        int idStatusBarHeight = myResources.getIdentifier(
                "status_bar_height", "dimen", "android");
        if (idStatusBarHeight > 0) {
            height = getResources().getDimensionPixelSize(idStatusBarHeight);
            //Toast.makeText(this, "Status Bar Height = " + height, Toast.LENGTH_LONG).show();
        } else {
            height = 0;
            //Toast.makeText(getActivity(), "Resources NOT found", Toast.LENGTH_LONG).show();
        }

        return height;
    }
}
