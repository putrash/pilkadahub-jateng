package com.simalakama.pilkadahubjateng.custom;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Window;
import android.widget.TextView;

import com.simalakama.pilkadahubjateng.R;

/**
 * Created by Putraa on 4/16/2018.
 */

public class CustomDialog extends Dialog {

    Context context;
    String informasi;

    public CustomDialog(Context context, String informasi){
        super(context);
        this.context = context;
        this.informasi = informasi;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_item_selesai);

        TextView textInformasi = findViewById(R.id.text_informasi);
        textInformasi.setText(informasi);

    }
}
