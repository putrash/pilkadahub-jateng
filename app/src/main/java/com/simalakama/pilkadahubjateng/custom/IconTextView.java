package com.simalakama.pilkadahubjateng.custom;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

/**
 * Created by Putraa on 4/19/2018.
 */

public class IconTextView extends android.support.v7.widget.AppCompatTextView {

    public IconTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public IconTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public IconTextView(Context context) {
        super(context);
        init();
    }

    private void init() {

        //Font name should not contain "/".
        Typeface typeface= Typeface.createFromAsset(getContext().getAssets(),
                "fontawesome.ttf");
        setTypeface(typeface);
    }

}
