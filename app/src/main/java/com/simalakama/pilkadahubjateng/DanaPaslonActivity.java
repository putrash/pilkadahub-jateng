package com.simalakama.pilkadahubjateng;

import android.content.Intent;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TextView;

import com.simalakama.pilkadahubjateng.adapter.ListDetailProfilAdapter;
import com.simalakama.pilkadahubjateng.data.ApiService;
import com.simalakama.pilkadahubjateng.model.Response.ModelPaslon;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DanaPaslonActivity extends AppCompatActivity {

    String[] textTitle = {"Laporan Awal Dana Kampanye",
                          "Tanggal Penyerahan",
                          "Waktu Penyerahan"};

    String[] textIsi = {"Rp. 0",
                        "14 Februari 2018",
                        "Pukul 13.49 WIB"};

    private String id, paslon;
    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dana_paslon);

        id = getIntent().getStringExtra("ID");
        if (id.equals("1")){
            id = "5ad04fccf637c50414aced74";
            paslon = "Ganjar - Yasin";
        } else if (id.equals("2")){
            id = "5ad04fccf637c50414aced75";
            paslon = "Sudirman - Ida";
        }

        // Set a Toolbar to replace the ActionBar and add Padding.
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setPadding(0,getToolBarHeight(),0,0);
        toolbar.setNavigationIcon(R.drawable.icon_close);

        TextView textPaslon = findViewById(R.id.text_paslon);
        textPaslon.setText(paslon);

        listView = findViewById(R.id.list_dana_paslon);

        getDetailPaslon();
    }

    void getDetailPaslon(){
        Call<ModelPaslon> paslonCall = ApiService.service.getDetailPaslon(id);
        paslonCall.enqueue(new Callback<ModelPaslon>() {
            @Override
            public void onResponse(Call<ModelPaslon> call, Response<ModelPaslon> response) {
                if (response.isSuccessful()){
                    Log.d("Successfull", String.valueOf(response.body()));

                    ModelPaslon modelPaslon = response.body();
                    textIsi[0] = modelPaslon.getDanaKampanye();

                    listView.setAdapter(new ListDetailProfilAdapter(DanaPaslonActivity.this,textTitle,textIsi));

                } else {
                    Log.d("onResponse but Failure", String.valueOf(response.code()));
                }
            }

            @Override
            public void onFailure(Call<ModelPaslon> call, Throwable t) {
                Log.d("onFailure", "Something goes wrong" + t.toString());
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private int getToolBarHeight() {
        int height;

        Resources myResources = getResources();
        int idStatusBarHeight = myResources.getIdentifier(
                "status_bar_height", "dimen", "android");
        if (idStatusBarHeight > 0) {
            height = getResources().getDimensionPixelSize(idStatusBarHeight);
            //Toast.makeText(this, "Status Bar Height = " + height, Toast.LENGTH_LONG).show();
        }else{
            height = 0;
            //Toast.makeText(getActivity(), "Resources NOT found", Toast.LENGTH_LONG).show();
        }

        return height;
    }
}
