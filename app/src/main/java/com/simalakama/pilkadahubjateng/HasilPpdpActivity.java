package com.simalakama.pilkadahubjateng;

import android.Manifest;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.simalakama.pilkadahubjateng.adapter.ListHasilPpdpAdapter;
import com.simalakama.pilkadahubjateng.data.ApiService;
import com.simalakama.pilkadahubjateng.model.HasilPpdb;
import com.simalakama.pilkadahubjateng.model.Response.ModelPpdb;
import com.simalakama.pilkadahubjateng.utils.ScreenShootUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HasilPpdpActivity extends AppCompatActivity implements ActivityCompat.OnRequestPermissionsResultCallback {

    public static final String KABUPATEN = "kabupaten";
    public static final String KECAMATAN = "kecamatan";
    public static final String KELURAHAN = "kelurahan";

    String kabupaten, kecamatan, kelurahan;
    private TextView textLokasi, textJumlah;

    private List<HasilPpdb> hasilPpdbList = new ArrayList<>();
    private ListHasilPpdpAdapter listHasilPpdbAdapter;
    private ListView listView;
    private View rootView;
    private Toolbar toolbar;
    private LinearLayout viewEmpty;
    private RelativeLayout viewProgress;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hasil_ppdp);

        rootView = getWindow().getDecorView().findViewById(android.R.id.content);

        //Get Intent Extra
        kabupaten = getIntent().getStringExtra(KABUPATEN);
        kecamatan = getIntent().getStringExtra(KECAMATAN);
        kelurahan = getIntent().getStringExtra(KELURAHAN);
        //Log.d("Successfull", "onCreate: " + kabupaten + " " + kecamatan + " " + kelurahan);

        initView();
        initToolbar();

        //Set Judul Lokasi
        String lokasi = kelurahan.substring(0, 1).toUpperCase() + kelurahan.substring(1).toLowerCase();
        textLokasi.setText(lokasi);

        viewProgress.setVisibility(View.VISIBLE);
        getHasilPpdb();

    }

    void initView() {
        toolbar = findViewById(R.id.toolbar);
        //
        textLokasi = findViewById(R.id.text_lokasi);
        textJumlah = findViewById(R.id.text_jumlahhasil);
        //
        listView = findViewById(R.id.list_ppdb);
        viewEmpty = findViewById(R.id.view_empty);
        viewProgress = findViewById(R.id.view_progress);
    }

    void initToolbar() {
        // Set a Toolbar to replace the ActionBar and add Padding.
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setPadding(0, getToolBarHeight(), 0, 0);
    }

    void getHasilPpdb() {
        Call<List<ModelPpdb>> ppdbCall = ApiService.service.getPpdb(kabupaten, kecamatan, kelurahan);
        ppdbCall.enqueue(new Callback<List<ModelPpdb>>() {
            @Override
            public void onResponse(Call<List<ModelPpdb>> call, Response<List<ModelPpdb>> response) {
                if (response.isSuccessful()) {

                    Log.d("Successfull", String.valueOf(response.body()));
                    List<ModelPpdb> modelPpdbList = response.body();
                    textJumlah.setText(String.valueOf(modelPpdbList.size()));

                    if (modelPpdbList.size() != 0) {
                        listView.setVisibility(View.VISIBLE);
                        viewEmpty.setVisibility(View.GONE);
                        for (int i = 0; i < modelPpdbList.size(); i++) {
                            String nomor = String.valueOf(i + 1);

                            hasilPpdbList.add(new HasilPpdb(nomor, modelPpdbList.get(i).getNama(), modelPpdbList.get(i).getTps()));
                            listHasilPpdbAdapter = new ListHasilPpdpAdapter(getApplicationContext(), hasilPpdbList);
                            listView.setAdapter(listHasilPpdbAdapter);
                        }
                    } else {
                        listView.setVisibility(View.GONE);
                        viewEmpty.setVisibility(View.VISIBLE);
                    }

                    viewProgress.setVisibility(View.GONE);

                } else {
                    Log.d("onResponse but Failure", String.valueOf(response.code()));
                }

            }

            @Override
            public void onFailure(Call<List<ModelPpdb>> call, Throwable t) {
                Log.d("onFailure", "Something goes wrong" + t.toString());
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_screenshoot, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                finish();
                break;
            case R.id.menu_screenshoot:

                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {

                    ActivityCompat.requestPermissions(HasilPpdpActivity.this,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            1);

                } else {
                    Log.i("Permission", "STORAGE permission has already been granted.\")");

                    Bitmap bitmap = ScreenShootUtils.getScreenShot(rootView);
                    if (bitmap != null) {

                        File file = ScreenShootUtils.store(bitmap, "Pilkadahub_" + System.currentTimeMillis() + ".jpg");
                        Toast.makeText(this, "You have successfully screenshoot", Toast.LENGTH_SHORT).show();
                        Log.d("Screenshoot", "createdAt: " + file);

                    } else {
                        Toast.makeText(this, "Opss Something wrong", Toast.LENGTH_SHORT).show();
                    }
                }

                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(HasilPpdpActivity.this, "Permission denied to read your External storage", Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }

    private int getToolBarHeight() {
        int height;

        Resources myResources = getResources();
        int idStatusBarHeight = myResources.getIdentifier(
                "status_bar_height", "dimen", "android");
        if (idStatusBarHeight > 0) {
            height = getResources().getDimensionPixelSize(idStatusBarHeight);
            //Toast.makeText(this, "Status Bar Height = " + height, Toast.LENGTH_LONG).show();
        } else {
            height = 0;
            //Toast.makeText(getActivity(), "Resources NOT found", Toast.LENGTH_LONG).show();
        }

        return height;
    }
}
