package com.simalakama.pilkadahubjateng.adapter;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.simalakama.pilkadahubjateng.DetailBeritaActivity;
import com.simalakama.pilkadahubjateng.R;
import com.simalakama.pilkadahubjateng.VideoFullscreenActivity;
import com.simalakama.pilkadahubjateng.model.Berita;
import com.simalakama.pilkadahubjateng.model.TataCara;
import com.simalakama.pilkadahubjateng.model.Video;
import com.squareup.picasso.Picasso;

import java.util.List;

import static com.simalakama.pilkadahubjateng.VideoFullscreenActivity.JUDUL_BERITA;
import static com.simalakama.pilkadahubjateng.VideoFullscreenActivity.VIDEO_BERITA;

/**
 * Created by Putraa on 5/4/2018.
 */

public class RecycleVideoBeritaAdapter extends RecyclerView.Adapter<RecycleVideoBeritaAdapter.ViewHolder> {

    private List<Video> videoList;

    public RecycleVideoBeritaAdapter(List<Video> listVideo){
        this.videoList = listVideo;
    }

    @Override
    public RecycleVideoBeritaAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycle_item_videoberita,parent,false);

        return new RecycleVideoBeritaAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecycleVideoBeritaAdapter.ViewHolder holder, int position) {
        Video video = videoList.get(position);

        Picasso.get()
                .load(video.getImageBerita())
                .resize(520,300)
                .placeholder(R.color.colorPrimaryDark)
                .into(holder.imageVideo);
        holder.textJudul.setText(video.getTextJudul());
        holder.textDuration.setText(video.getTextDuration());


    }

    public void clear() {
        videoList.clear();
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return videoList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView imageVideo;
        TextView textJudul, textDuration;

        ViewHolder(View view){
            super(view);
            imageVideo = view.findViewById(R.id.image_cover);
            textJudul = view.findViewById(R.id.text_judul);
            textDuration = view.findViewById(R.id.text_duration);
            view.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(v.getContext(), VideoFullscreenActivity.class);
            intent.putExtra(JUDUL_BERITA,videoList.get(getAdapterPosition()).getTextJudul());
            intent.putExtra(VIDEO_BERITA,videoList.get(getAdapterPosition()).getVideoBerita());
            v.getContext().startActivity(intent);
        }
    }
}
