package com.simalakama.pilkadahubjateng.adapter;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.simalakama.pilkadahubjateng.DetailBeritaActivity;
import com.simalakama.pilkadahubjateng.R;
import com.simalakama.pilkadahubjateng.model.Berita;
import com.squareup.picasso.Picasso;

import java.util.List;


/**
 * Created by Putraa on 4/14/2018.
 */

public class RecycleBeritaLainnyaAdapter extends RecyclerView.Adapter<RecycleBeritaLainnyaAdapter.ViewHolder> {

    private List<Berita> beritaList;

    public RecycleBeritaLainnyaAdapter(List<Berita> beritaList){
        this.beritaList = beritaList;
    }

    @Override
    public RecycleBeritaLainnyaAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycle_item_beritalainnya,parent,false);

        return new RecycleBeritaLainnyaAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecycleBeritaLainnyaAdapter.ViewHolder holder, int position) {
        Berita berita = beritaList.get(position);

        Picasso.get()
                .load(berita.getImageBerita())
                .placeholder(R.drawable.icon_placeholder)
                .fit()
                .into(holder.imageBerita);
        holder.textTanggal.setText(berita.getTextTanggal());
        holder.textTitleBerita.setText(berita.getTextTitleBerita());
        holder.textDetailBerita.setText(berita.getTextDetailBerita());
    }

    public void clear() {
        beritaList.clear();
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return beritaList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView textTanggal, textDetailBerita, textTitleBerita;
        public ImageView imageBerita;

        public ViewHolder(View view){
            super(view);
            imageBerita = view.findViewById(R.id.image_beritalainnya);
            textTanggal = view.findViewById(R.id.text_tanggalberitalainnya);
            textTitleBerita = view.findViewById(R.id.text_titleberita);
            textDetailBerita = view.findViewById(R.id.text_detailberita);
            view.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(v.getContext(), DetailBeritaActivity.class);
            intent.putExtra(DetailBeritaActivity.TITLE_BERITA,beritaList.get(getAdapterPosition()).getTextTitleBerita());
            intent.putExtra(DetailBeritaActivity.ISI_BERITA,beritaList.get(getAdapterPosition()).getTextDetailBerita());
            intent.putExtra(DetailBeritaActivity.TANGGAL_BERITA,beritaList.get(getAdapterPosition()).getTextTanggal());
            intent.putExtra(DetailBeritaActivity.COVER_BERITA,beritaList.get(getAdapterPosition()).getImageBerita());
            v.getContext().startActivity(intent);

            //Toast.makeText(v.getContext(), "Status Bar Height = " + beritaList.get(getAdapterPosition()).getTextTitleBerita(), Toast.LENGTH_LONG).show();


        }
    }
}
