package com.simalakama.pilkadahubjateng.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.simalakama.pilkadahubjateng.R;
import com.simalakama.pilkadahubjateng.model.HasilPpdb;

import java.util.List;

/**
 * Created by Putraa on 4/18/2018.
 */

public class ListHasilPpdpAdapter extends BaseAdapter {

    private Context context;
    private List<HasilPpdb> ppdbList;

    public ListHasilPpdpAdapter(Context context, List<HasilPpdb> ppdbList){
        this.context = context;
        this.ppdbList = ppdbList;
    }

    @Override
    public int getCount() {
        return ppdbList.size() ;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    private static class ViewHolder {
        TextView textNo;
        TextView textNama;
        TextView textTPS;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        View view = convertView;
        ViewHolder holder;

        if (view == null){
            holder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.list_item_ppdp,null);
            holder.textNo = view.findViewById(R.id.text_nomor);
            holder.textNama = view.findViewById(R.id.text_nama);
            holder.textTPS = view.findViewById(R.id.text_tps);
            view.setTag(holder);

            if (i % 2 == 0){
                view.setBackgroundResource(R.color.colorWhite);
            } else {
                view.setBackgroundResource(R.color.colorBackgroundList);
            }

        } else {
            holder = (ViewHolder) view.getTag();
        }

        holder.textNo.setText(ppdbList.get(i).getTextNo());
        holder.textNama.setText(ppdbList.get(i).getTextNama());
        holder.textTPS.setText(ppdbList.get(i).getTextTPS());

        return view;
    }
}
