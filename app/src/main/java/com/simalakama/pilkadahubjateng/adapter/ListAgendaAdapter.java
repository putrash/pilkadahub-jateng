package com.simalakama.pilkadahubjateng.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.simalakama.pilkadahubjateng.R;
import com.simalakama.pilkadahubjateng.model.Agenda;

import java.util.List;

/**
 * Created by Putraa on 4/12/2018.
 */

public class ListAgendaAdapter extends BaseAdapter {

    private Context context;
    private List<Agenda> agendaList;

    public ListAgendaAdapter(Context context, List<Agenda> agendaList){
        this.context = context;
        this.agendaList = agendaList;
    }

    @Override
    public int getCount() {
        return agendaList.size() ;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    private static class ViewHolder {
        TextView textAgenda;
        TextView textTanggalAwal;
        TextView textTanggalAkhir;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        View view = convertView;
        ViewHolder holder;

        if (view == null){
            holder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.list_item_agenda,null);
            holder.textAgenda = view.findViewById(R.id.text_agenda);
            holder.textTanggalAwal = view.findViewById(R.id.text_tanggalawal);
            holder.textTanggalAkhir = view.findViewById(R.id.text_tanggalakhir);
            view.setTag(holder);

        } else {
            holder = (ViewHolder) view.getTag();
        }

        holder.textAgenda.setText(agendaList.get(i).getTextAgenda());
        holder.textTanggalAwal.setText(agendaList.get(i).getTextTanggalAwal());
        holder.textTanggalAkhir.setText(agendaList.get(i).getTextTanggalAkhir());

        return view;
    }
}
