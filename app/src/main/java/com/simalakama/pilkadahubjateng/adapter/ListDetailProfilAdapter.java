package com.simalakama.pilkadahubjateng.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.simalakama.pilkadahubjateng.R;

/**
 * Created by Putraa on 4/18/2018.
 */

public class ListDetailProfilAdapter extends BaseAdapter {

    private Context context;
    private String[] textTitle;
    private String[] textIsi;


    public ListDetailProfilAdapter(Context context, String[] textTitle, String[] textIsi){
        this.context = context;
        this.textTitle = textTitle;
        this.textIsi = textIsi;
    }

    @Override
    public int getCount() {
        return textTitle.length ;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    private static class ViewHolder {
        TextView textTitle;
        TextView textIsi;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        View view = convertView;
        ListDetailProfilAdapter.ViewHolder holder;

        if (view == null){
            holder = new ListDetailProfilAdapter.ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.list_item_detail_profil,null);
            holder.textTitle = (TextView) view.findViewById(R.id.text_title);
            holder.textIsi = (TextView) view.findViewById(R.id.text_isi);
            view.setTag(holder);

        } else {
            holder = (ListDetailProfilAdapter.ViewHolder) view.getTag();
        }

        holder.textTitle.setText(textTitle[i]);
        holder.textIsi.setText(textIsi[i]);

        return view;
    }


}
