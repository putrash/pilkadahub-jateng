package com.simalakama.pilkadahubjateng.adapter;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.simalakama.pilkadahubjateng.DetailBeritaActivity;
import com.simalakama.pilkadahubjateng.R;
import com.simalakama.pilkadahubjateng.model.Berita;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Putraa on 4/13/2018.
 */

public class RecycleBeritaAdapter extends RecyclerView.Adapter<RecycleBeritaAdapter.ViewHolder> {

    private List<Berita> beritaList;

    public RecycleBeritaAdapter(List<Berita> beritaList){
        this.beritaList = beritaList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.recycle_item_berita,parent,false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Berita berita = beritaList.get(position);

        Picasso.get()
                .load(berita.getImageBerita())
                .placeholder(R.drawable.icon_placeholder)
                .fit()
                .into(holder.imageBerita);
        holder.textTanggal.setText(berita.getTextTanggal());
        holder.textBerita.setText(berita.getTextTitleBerita());
    }

    @Override
    public int getItemCount() {
        return beritaList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView textTanggal, textBerita;
        public ImageView imageBerita;

        public ViewHolder(View view){
            super(view);
            imageBerita = view.findViewById(R.id.image_berita);
            textTanggal = view.findViewById(R.id.text_tanggalberita);
            textBerita = view.findViewById(R.id.text_berita);
            view.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(v.getContext(), DetailBeritaActivity.class);
            intent.putExtra(DetailBeritaActivity.TITLE_BERITA,beritaList.get(getAdapterPosition()).getTextTitleBerita());
            intent.putExtra(DetailBeritaActivity.ISI_BERITA,beritaList.get(getAdapterPosition()).getTextDetailBerita());
            intent.putExtra(DetailBeritaActivity.TANGGAL_BERITA,beritaList.get(getAdapterPosition()).getTextTanggal());
            intent.putExtra(DetailBeritaActivity.COVER_BERITA,beritaList.get(getAdapterPosition()).getImageBerita());
            v.getContext().startActivity(intent);

            //Toast.makeText(v.getContext(), "Status Bar Height = " + beritaList.get(getAdapterPosition()).getTextTitleBerita(), Toast.LENGTH_LONG).show();

        }
    }
}
