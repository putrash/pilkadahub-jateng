package com.simalakama.pilkadahubjateng.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.simalakama.pilkadahubjateng.R;
import com.simalakama.pilkadahubjateng.model.TataCara;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Putraa on 4/15/2018.
 */

public class RecycleCaraPilkadaAdapter extends RecyclerView.Adapter<RecycleCaraPilkadaAdapter.ViewHolder> {

    private List<TataCara> tataCaraList;

    public RecycleCaraPilkadaAdapter(List<TataCara> listTataCara){
        this.tataCaraList = listTataCara;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.recycle_item_tatacara,parent,false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        TataCara tataCara = tataCaraList.get(position);

        Picasso.get()
                .load(tataCara.getImageTataCara())
                .resize(520,300)
                .placeholder(R.color.colorPrimaryDark)
                .into(holder.imageTataCara);
        holder.textUrutan.setText(String.valueOf(tataCara.getTextUrutan()));


    }

    @Override
    public int getItemCount() {
        return tataCaraList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView imageTataCara;
        TextView textUrutan;

        ViewHolder(View view){
            super(view);
            imageTataCara = view.findViewById(R.id.image_tatacara);
            textUrutan = view.findViewById(R.id.text_urutan);
            view.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            //Toast.makeText(v.getContext(), "Tata Cara",Toast.LENGTH_LONG).show();

        }
    }
}
