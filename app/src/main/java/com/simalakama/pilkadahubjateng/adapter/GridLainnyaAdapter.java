package com.simalakama.pilkadahubjateng.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.simalakama.pilkadahubjateng.R;

/**
 * Created by Putraa on 4/12/2018.
 */

public class GridLainnyaAdapter extends BaseAdapter {

    private Context context;
    private String[] textItems;
    private int[] imageItems;


    public GridLainnyaAdapter (Context context, String[] textItems, int[] imageItems){
        this.context = context;
        this.textItems = textItems;
        this.imageItems = imageItems;
    }

    @Override
    public int getCount() {
        return textItems.length ;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    private static class ViewHolder {
        TextView textItems;
        ImageView imageItems;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        View view = convertView;
        ViewHolder holder;

        if (view == null){
            holder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.grid_item_lainnya,null);
            holder.textItems = view.findViewById(R.id.text_menu);
            holder.imageItems = view.findViewById(R.id.image_icon);
            view.setTag(holder);

        } else {
            holder = (ViewHolder)view.getTag();
        }

        holder.textItems.setText(textItems[i]);
        holder.imageItems.setImageResource(imageItems[i]);

        return view;
    }
}
