package com.simalakama.pilkadahubjateng.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.simalakama.pilkadahubjateng.R;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Putraa on 4/14/2018.
 */

public class ListBantuanAdapter extends BaseExpandableListAdapter {

    private Context context;
    private List<String> textHeaderTitle;
    private HashMap<String,List<String>> textDetail;

    public ListBantuanAdapter(Context context, List<String> textHeaderTitle, HashMap<String,List<String>> textDetail){
        this.context = context;
        this.textHeaderTitle = textHeaderTitle;
        this.textDetail = textDetail;
    }

    @Override
    public Object getChild(int i, int i1) {
        return this.textDetail.get(textHeaderTitle.get(i)).get(i1);
    }

    @Override
    public long getChildId(int i, int i1) {
        return i1;
    }

    @Override
    public View getChildView(int i, int i1, boolean b, View view, ViewGroup viewGroup) {
        final String expandableTextDetail = (String)getChild(i,i1);
        if (view == null){
            LayoutInflater layoutInflater = (LayoutInflater)this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.list_item_bantuan,null);
        }

        TextView textDetail = view.findViewById(R.id.text_detailbantuan);
        textDetail.setText(expandableTextDetail);
        return view;
    }

    @Override
    public int getChildrenCount(int i) {
        return this.textDetail.get(this.textHeaderTitle.get(i)).size();
    }

    @Override
    public Object getGroup(int i) {
        return this.textHeaderTitle.get(i);
    }

    @Override
    public int getGroupCount() {
        return this.textHeaderTitle.size();
    }

    @Override
    public long getGroupId(int i) {
        return i;

    }

    @Override
    public View getGroupView(int i, boolean b, View view, ViewGroup viewGroup) {
        String expandableTextHeader = (String)getGroup(i);
        if (view == null){
            LayoutInflater layoutInflater = (LayoutInflater)this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.list_header_bantuan,null);
        }
        TextView textHeader = view.findViewById(R.id.text_headerbantuan);
        textHeader.setText(expandableTextHeader);
        return view;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        return true;
    }
}
