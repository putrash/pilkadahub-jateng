package com.simalakama.pilkadahubjateng.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.simalakama.pilkadahubjateng.JawabanActivity;
import com.simalakama.pilkadahubjateng.R;
import com.simalakama.pilkadahubjateng.model.Forum;

import java.util.List;

import static android.content.ContentValues.TAG;

/**
 * Created by Putraa on 4/12/2018.
 */

public class ListForumAdapter extends BaseAdapter {

    private Context context;
    private List<Forum> forumList;

    public ListForumAdapter(Context context, List<Forum> forumList){
        this.context = context;
        this.forumList = forumList;
    }

    @Override
    public int getCount() {
        return forumList.size() ;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    private static class ViewHolder {
        TextView txNama;
        TextView txTanggal;
        TextView txPertanyaan;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        View view = convertView;
        ViewHolder holder;

        if (view == null){
            holder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.list_item_forum,null);
            holder.txNama = view.findViewById(R.id.nama);
            holder.txTanggal = view.findViewById(R.id.tanggal);
            holder.txPertanyaan = view.findViewById(R.id.pertanyaan);
            view.setTag(holder);

        } else {
            holder = (ViewHolder) view.getTag();
        }
        String nama = forumList.get(i).getNama();
        String tanggal = forumList.get(i).getTanggal();
        String pertanyaan = forumList.get(i).getPertanyaan();
        String tanyaKe = forumList.get(i).getTanyaKe();
        String id = forumList.get(i).getIdPertanyaan();

        holder.txNama.setText(nama);
        holder.txTanggal.setText(tanggal);
        holder.txPertanyaan.setText(pertanyaan);
//        intent onclick
        view.setOnClickListener(v -> {
            Intent intent = new Intent(context, JawabanActivity.class);
            intent.putExtra("nama", nama);
            intent.putExtra("tanggal", tanggal);
            intent.putExtra("pertanyaan", pertanyaan);
            intent.putExtra("tanyake", tanyaKe);
            intent.putExtra("id", id);
            context.startActivity(intent);
        });
        return view;
    }
}
