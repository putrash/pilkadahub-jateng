package com.simalakama.pilkadahubjateng.fragment;

import android.content.Intent;
import android.content.res.Resources;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.simalakama.pilkadahubjateng.R;
import com.simalakama.pilkadahubjateng.TambahPertanyaanActivity;
import com.simalakama.pilkadahubjateng.adapter.TabAdapter;

public class ForumFragment extends Fragment {

    public static ForumFragment newInstance() {
        // Required empty public constructor

        return new ForumFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_forum, container, false);
        setHasOptionsMenu(true);

        // Set a Toolbar to replace the ActionBar and add Padding.
        Toolbar toolbar = rootView.findViewById(R.id.toolbar);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setPadding(0,getToolBarHeight(),0,0);

        TabAdapter tabAdapter = new TabAdapter(getChildFragmentManager());
        tabAdapter.addFragment(new ForumSemuaFragment(),"Semua");
        tabAdapter.addFragment(new ForumPaslon1Fragment(),"Paslon 1");
        tabAdapter.addFragment(new ForumPaslon2Fragment(),"Paslon 2");

        ViewPager viewPager = rootView.findViewById(R.id.viewpager_forum);
        viewPager.setAdapter(tabAdapter);

        TabLayout tabLayout = rootView.findViewById(R.id.tab_forum);
        tabLayout.setTabTextColors(getResources().getColor(R.color.colorTitleTab),getResources().getColor(android.R.color.white));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.setupWithViewPager(viewPager);

        FloatingActionButton fab = rootView.findViewById(R.id.fab);

        fab.setOnClickListener(v -> startActivity(new Intent(getActivity(), TambahPertanyaanActivity.class)));

        return rootView;

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        menuInflater.inflate(R.menu.menu_lain, menu);
        super.onCreateOptionsMenu(menu,menuInflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.menu_lain:
                BottomSheetDialogFragment bottomSheetDialogFragment = new ButtonSheetDialogFragment();
                bottomSheetDialogFragment.show(getChildFragmentManager(),bottomSheetDialogFragment.getTag());
        }


        return super.onOptionsItemSelected(item);
    }


    private int getToolBarHeight() {
        int height;

        Resources myResources = getResources();
        int idStatusBarHeight = myResources.getIdentifier(
                "status_bar_height", "dimen", "android");
        if (idStatusBarHeight > 0) {
            height = getResources().getDimensionPixelSize(idStatusBarHeight);
            //Toast.makeText(getActivity(), "Status Bar Height = " + height, Toast.LENGTH_LONG).show();
        }else{
            height = 0;
            //Toast.makeText(getActivity(), "Resources NOT found", Toast.LENGTH_LONG).show();
        }

        return height;
    }
}
