package com.simalakama.pilkadahubjateng.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.simalakama.pilkadahubjateng.R;
import com.simalakama.pilkadahubjateng.adapter.ListAgendaAdapter;
import com.simalakama.pilkadahubjateng.data.ApiService;
import com.simalakama.pilkadahubjateng.model.Agenda;
import com.simalakama.pilkadahubjateng.model.Response.ModelAgenda;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class AgendaPenyelenggaraanFragment extends Fragment {

    private List<Agenda> agendaList = new ArrayList<>();
    private ListAgendaAdapter listAgendaAdapter;
    private ShimmerFrameLayout shimmerView;
    private ListView listView;
    private String tanggalAwal, tanggalAkhir;
    private Locale locale;
    private SimpleDateFormat simpleDateFormatOne, simpleDateFormatTwo;

    public AgendaPenyelenggaraanFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_agenda_penyelenggaraan, container, false);

        shimmerView = rootView.findViewById(R.id.view_shimer);
        listView = rootView.findViewById(R.id.list_agenda_penyelenggaraan);

        getAgendaPenyelenggaraan();

        return rootView;
    }

    void getAgendaPenyelenggaraan() {
        Call<List<ModelAgenda>> agendaCall = ApiService.service.getAgenda();
        agendaCall.enqueue(new Callback<List<ModelAgenda>>() {
            @Override
            public void onResponse(Call<List<ModelAgenda>> call, Response<List<ModelAgenda>> response) {
                if (response.isSuccessful()) {

                    //Log.d("Successfull", String.valueOf(response.body()));
                    List<ModelAgenda> modelAgendaList = response.body();

                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                        locale = new Locale.Builder().setLanguage("id").setRegion("ID").build();
                        simpleDateFormatOne = new SimpleDateFormat("dd-MMM-yyyy",locale);
                        simpleDateFormatTwo = new SimpleDateFormat("dd MMMM yyyy",locale);
                    } else {
                        simpleDateFormatOne = new SimpleDateFormat("dd-MMM-yyyy");
                        simpleDateFormatTwo = new SimpleDateFormat("dd MMMM yyyy");
                    }

                    for (int i = 0; i < modelAgendaList.size(); i++) {

                        if (modelAgendaList.get(i).getTahapan().equals("Penyelenggaraan")) {
                            String stringTanggalAwal = modelAgendaList.get(i).getTgl_awal();
                            String stringTanggalAkhir = modelAgendaList.get(i).getTgl_akhir();
                            SimpleDateFormat simpleDateFormat = simpleDateFormatOne;
                            try {
                                Date dateTanggalAwal = simpleDateFormat.parse(stringTanggalAwal);
                                Date dateTanggalAkhir = simpleDateFormat.parse(stringTanggalAkhir);
                                simpleDateFormat = simpleDateFormatTwo;
                                tanggalAwal = simpleDateFormat.format(dateTanggalAwal);
                                tanggalAkhir = simpleDateFormat.format(dateTanggalAkhir);
                                //Log.d("onFormat: ", tanggalAwal + " " +tanggalAkhir);

                            } catch (ParseException e){
                                e.printStackTrace();
                            }
                            agendaList.add(new Agenda(tanggalAwal, tanggalAkhir, modelAgendaList.get(i).getDetail()));
                            listAgendaAdapter = new ListAgendaAdapter(getActivity(), agendaList);
                            listView.setAdapter(listAgendaAdapter);
                        }
                    }

                    shimmerView.stopShimmerAnimation();
                    shimmerView.setVisibility(View.GONE);

                } else {
                    Log.d("onResponse but Failure", String.valueOf(response.code()));
                }
            }

            @Override
            public void onFailure(Call<List<ModelAgenda>> call, Throwable t) {
                Log.d("onFailure", "Something goes wrong" + t.toString());
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        shimmerView.startShimmerAnimation();
    }

    @Override
    public void onPause() {
        shimmerView.stopShimmerAnimation();
        super.onPause();
    }
}
