package com.simalakama.pilkadahubjateng.fragment;


import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import com.simalakama.pilkadahubjateng.AgendaActivity;
import com.simalakama.pilkadahubjateng.BantuanActivity;
import com.simalakama.pilkadahubjateng.BeritaPilkadaActivity;
import com.simalakama.pilkadahubjateng.CaraPilkadaActivity;
import com.simalakama.pilkadahubjateng.CekHasilPilkadaActivity;
import com.simalakama.pilkadahubjateng.PengaduanActivity;
import com.simalakama.pilkadahubjateng.R;
import com.simalakama.pilkadahubjateng.TentangActivity;
import com.simalakama.pilkadahubjateng.VideoActivity;
import com.simalakama.pilkadahubjateng.adapter.GridLainnyaAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class LainnyaFragment extends Fragment {

    String[] textMenu = {"Cek Hasil Pilkada", "Pengaduan",
                         "Berita Pilkada", "Agenda Pilkada",
                         "Video Pilkada", "Tata Cara Pilkada",
                         "Bantuan", "Tentang Kami"};

    int[] imageIcon = {R.drawable.icon_elections,R.drawable.icon_bigdemostration,
                       R.drawable.icon_newspaper,R.drawable.icon_bigagenda,
                       R.drawable.icon_video, R.drawable.icon_bigstudying,
                       R.drawable.icon_question,R.drawable.icon_letteri};
    private View rootView;
    private Toolbar toolbar;
    private GridView gridView;

    public static LainnyaFragment newInstance() {
        // Required empty public constructor
        return new LainnyaFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_lainnya, container, false);
        setHasOptionsMenu(true);

        initView();
        initToolbar();

        return rootView;
    }

    void initView(){
        toolbar = rootView.findViewById(R.id.toolbar);
        //
        gridView = rootView.findViewById(R.id.grid_lainnya);
        gridView.setAdapter(new GridLainnyaAdapter(getActivity(),textMenu,imageIcon));
    }

    void initToolbar(){
        // Set a Toolbar to replace the ActionBar and add Padding.
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setPadding(0,getToolBarHeight(),0,0);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Class selectedClass = null;
                // TODO: 4/13/2018 GANTI CLASS UNTUK SETIAP CASE
                switch (i){

                    case 0 :
                        //Cek Hasil Pilkada
                        selectedClass = CekHasilPilkadaActivity.class;
                        break;
                    case 1 :
                        //Pengaduan
                        selectedClass = PengaduanActivity.class;
                        break;
                    case 2 :
                        //Berita Pilkada
                        selectedClass = BeritaPilkadaActivity.class;
                        break;
                    case 3 :
                        //Agenda Pilkada
                        selectedClass = AgendaActivity.class;
                        break;
                    case 4 :
                        //Video Pilkada
                        selectedClass = VideoActivity.class;
                        break;
                    case 5 :
                        //Tata Cara Pilkada
                        selectedClass = CaraPilkadaActivity.class;
                        break;
                    case 6:
                        //Bantuan
                        selectedClass = BantuanActivity.class;
                        break;
                    case 7:
                        //Tentang Kami
                        selectedClass = TentangActivity.class;
                        break;
                }

                Intent intent = new Intent(getActivity(),selectedClass);
                startActivity(intent);

            }
        });

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        menuInflater.inflate(R.menu.menu_lain, menu);
        super.onCreateOptionsMenu(menu,menuInflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.menu_lain:
                BottomSheetDialogFragment bottomSheetDialogFragment = new ButtonSheetDialogFragment();
                bottomSheetDialogFragment.show(getChildFragmentManager(),bottomSheetDialogFragment.getTag());
        }


        return super.onOptionsItemSelected(item);
    }

    private int getToolBarHeight() {
        int height;

        Resources myResources = getResources();
        int idStatusBarHeight = myResources.getIdentifier(
                "status_bar_height", "dimen", "android");
        if (idStatusBarHeight > 0) {
            height = getResources().getDimensionPixelSize(idStatusBarHeight);
//            Toast.makeText(getActivity(), "Status Bar Height = " + height, Toast.LENGTH_LONG).show();
        }else{
            height = 0;
            //Toast.makeText(getActivity(), "Resources NOT found", Toast.LENGTH_LONG).show();
        }

        return height;
    }

}
