package com.simalakama.pilkadahubjateng.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.simalakama.pilkadahubjateng.R;
import com.simalakama.pilkadahubjateng.adapter.ListDetailProfilAdapter;
import com.simalakama.pilkadahubjateng.data.ApiService;
import com.simalakama.pilkadahubjateng.model.Response.ModelPaslon;
import com.squareup.picasso.Picasso;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfilWakilGubernurFragment extends Fragment {

    String[] textTitle = {"Jenis Kelamin","Tempat Lahir",
            "Tanggal Lahir","Pekerjaan Kepala Daerah","Jumlah Dukungan",
            "Status Petahana","Partai Pendukung"};

    String[] textIsi = {"","", "","","0","",""};

    private String id;
    private ListView listView;
    private TextView textNamaCawagub;
    private int profile;
    private View headerView;


    public ProfilWakilGubernurFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_profil_wakil_gubernur, container, false);

        id =  getArguments().getString("ID");
        if (id.equals("1")){
            id = "5ad04fccf637c50414aced74";
            profile = R.drawable.icon_wagubsatu;
        } else if (id.equals("2")){
            id = "5ad04fccf637c50414aced75";
            profile = R.drawable.icon_wagubdua;
        }

        listView = (ListView)rootView.findViewById(R.id.list_detail_profil);
        headerView = inflater.inflate(R.layout.header_list_profilwakilgubernur,listView,false);

        getDetailPaslon();

        // Init UI
        ImageButton buttonTwitter = headerView.findViewById(R.id.btn_twitter);
        buttonTwitter.setImageResource(R.drawable.icon_twitter);

        ImageButton buttonFacebook = headerView.findViewById(R.id.btn_facebook);
        buttonFacebook.setImageResource(R.drawable.icon_facebook);

        ImageButton buttonInstagram = headerView.findViewById(R.id.btn_instagram);
        buttonInstagram.setImageResource(R.drawable.icon_instagram);

        textNamaCawagub = headerView.findViewById(R.id.text_namacawagub);

        ImageView imageProfile = headerView.findViewById(R.id.image_profilwakilgubernur);
        Picasso.get()
                .load(profile)
                .into(imageProfile);
        imageProfile.setScaleType(ImageView.ScaleType.CENTER_CROP);


        return rootView;
    }

    void getDetailPaslon(){
        Call<ModelPaslon> paslonCall = ApiService.service.getDetailPaslon(id);
        paslonCall.enqueue(new Callback<ModelPaslon>() {
            @Override
            public void onResponse(Call<ModelPaslon> call, Response<ModelPaslon> response) {
                if (response.isSuccessful()){
                    Log.d("Successfull", String.valueOf(response.body()));

                    ModelPaslon modelPaslon = response.body();
                    textNamaCawagub.setText(modelPaslon.getNamaWakilKepalaDaerah());
                    textIsi[0] = modelPaslon.getGenderWakilKepalaDaerah();
                    textIsi[1] = modelPaslon.getTempatLahirWakilKepalaDaerah();
                    textIsi[2] = modelPaslon.getTanggalLahirWakilKepalaDaerah();
                    textIsi[3] = modelPaslon.getPekerjaanWakilKepalaDaerah().substring(0,11);
                    textIsi[4] = modelPaslon.getJumlahDukungan();
                    textIsi[5] = modelPaslon.getStatusPertahana();
                    textIsi[6] = modelPaslon.getPartaiPendukung();

                    listView.setAdapter(new ListDetailProfilAdapter(getActivity(),textTitle,textIsi));
                    listView.addHeaderView(headerView);
//                    getListView(listView);

                } else {
                    Log.d("onResponse but Failure", String.valueOf(response.code()));
                }
            }

            @Override
            public void onFailure(Call<ModelPaslon> call, Throwable t) {
                Log.d("onFailure", "Something goes wrong" + t.toString());
            }
        });
    }

    public static void getListView(ListView listView) {

        ListAdapter adapter = listView.getAdapter();

        if (adapter == null) {
            return;
        }
        ViewGroup vg = listView;
        int totalHeight = 0;
        for (int i = 0; i < adapter.getCount(); i++) {
            View listItem = adapter.getView(i, null, vg);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams par = listView.getLayoutParams();
        par.height = totalHeight + (listView.getDividerHeight() * (adapter.getCount() - 1));
        listView.setLayoutParams(par);
        listView.requestLayout();
    }

}
