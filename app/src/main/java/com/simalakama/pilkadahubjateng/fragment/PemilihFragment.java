package com.simalakama.pilkadahubjateng.fragment;


import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.simalakama.pilkadahubjateng.CariDpsActivity;
import com.simalakama.pilkadahubjateng.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class PemilihFragment extends Fragment {


    private View rootView;
    private Button btnPemilihSementara, btnPemilihTetap, btnRekap;
    private Toolbar toolbar;

    public static PemilihFragment newInstance() {
        // Required empty public constructor
        return new PemilihFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_pemilih, container, false);
        setHasOptionsMenu(true);

        initView();
        initToolbar();

        return rootView;
    }

    void initView(){
        // Set a Toolbar to replace the ActionBar and add Padding.
        toolbar = rootView.findViewById(R.id.toolbar);

        //Initiate Button
        btnPemilihSementara = rootView.findViewById(R.id.btn_dps);
        btnPemilihTetap = rootView.findViewById(R.id.btn_dpt);
        btnRekap = rootView.findViewById(R.id.btn_rekap);
    }
    
    void initToolbar(){
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setPadding(0,getToolBarHeight(),0,0);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        btnPemilihSementara.setOnClickListener(view -> {

            //For activity
            Intent intent = new Intent(getActivity(), CariDpsActivity.class);
            startActivity(intent);
            //getActivity().overridePendingTransition(R.anim.fade_in,R.anim.fade_out);

        });

        btnPemilihTetap.setOnClickListener(view -> {
            showMessageBox("Fitur sedang dalam proses pengembangan, Mohon bersabar.", "Gagal");
        });

        btnRekap.setOnClickListener(view -> {
            showMessageBox("Fitur sedang dalam proses pengembangan, Mohon bersabar.", "Gagal");
        });


    }

    private void showMessageBox(String message, String title) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity(),R.style.customAlertDialog);
        alertDialogBuilder.setTitle(title);
        alertDialogBuilder.setMessage(message);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton("OK", (dialogInterface, i) -> {
            dialogInterface.dismiss();
        });
        alertDialogBuilder.show();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        menuInflater.inflate(R.menu.menu_lain, menu);
        super.onCreateOptionsMenu(menu,menuInflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.menu_lain:
                BottomSheetDialogFragment bottomSheetDialogFragment = new ButtonSheetDialogFragment();
                bottomSheetDialogFragment.show(getChildFragmentManager(),bottomSheetDialogFragment.getTag());
        }
        return super.onOptionsItemSelected(item);
    }

    private int getToolBarHeight() {
        int height;

        Resources myResources = getResources();
        int idStatusBarHeight = myResources.getIdentifier(
                "status_bar_height", "dimen", "android");
        if (idStatusBarHeight > 0) {
            height = getResources().getDimensionPixelSize(idStatusBarHeight);
            //Toast.makeText(getActivity(), "Status Bar Height = " + height, Toast.LENGTH_LONG).show();
        }else{
            height = 0;
            //Toast.makeText(getActivity(), "Resources NOT found", Toast.LENGTH_LONG).show();
        }

        return height;
    }

}
