package com.simalakama.pilkadahubjateng.fragment;


import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.content.res.AppCompatResources;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.github.rubensousa.gravitysnaphelper.GravitySnapHelper;
import com.simalakama.pilkadahubjateng.AgendaActivity;
import com.simalakama.pilkadahubjateng.BeritaPilkadaActivity;
import com.simalakama.pilkadahubjateng.CaraPilkadaActivity;
import com.simalakama.pilkadahubjateng.CekHasilPilkadaActivity;
import com.simalakama.pilkadahubjateng.DetailPaslonActivity;
import com.simalakama.pilkadahubjateng.PengaduanActivity;
import com.simalakama.pilkadahubjateng.R;
import com.simalakama.pilkadahubjateng.adapter.RecycleBeritaAdapter;
import com.simalakama.pilkadahubjateng.custom.CustomDividerItemDecoration;
import com.simalakama.pilkadahubjateng.data.ApiService;
import com.simalakama.pilkadahubjateng.model.Berita;
import com.simalakama.pilkadahubjateng.model.Response.ModelBerita;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.simalakama.pilkadahubjateng.DetailPaslonActivity.ID_PASLON;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {

    private List<Berita> beritaList = new ArrayList<>();
    CountDownTimer countDownTimer = null;
    private TextView hari, jam, menit, detik, title;
    private long startTime;
    private long milliseconds;
    private RecycleBeritaAdapter recycleBeritaAdapter;
    private String tanggal, isiBerita;
    private Button buttonDetailPaslonSatu, buttonDetailPaslonDua, buttonPengaduan, buttonTataCara, buttonCekHasil, buttonAgenda;
    private View rootView, viewSemuaBerita, viewOnGoing, viewDone;
    private ShimmerFrameLayout shimmerView;
    private Toolbar toolbar;
    private CardView cardViewPaslonSatu, cardViewPaslonDua;
    private RecyclerView recyclerView;

    public static HomeFragment newInstance() {
        // Required empty public constructor

        return new HomeFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_home, container, false);
        setHasOptionsMenu(true);

        initView();
        initUI();
        startTimer();

        //Get Data
        getBeritaPopuler();

        return rootView;

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //Detail Paslon Satu
        buttonDetailPaslonSatu.setOnClickListener(view -> {
            Intent intent = new Intent(getActivity(), DetailPaslonActivity.class);
            intent.putExtra(ID_PASLON,"1");
            startActivity(intent);
        });

        //Detail Paslon Dua
        buttonDetailPaslonDua.setOnClickListener(view -> {
            Intent intent = new Intent(getActivity(), DetailPaslonActivity.class);
            intent.putExtra(ID_PASLON,"2");
            startActivity(intent);
        });

        //Cek Hasil Pilkada
        buttonCekHasil.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(getActivity(),R.drawable.icon_smallelections), null, null, null);
        buttonCekHasil.setOnClickListener(view -> {
            startActivity(new Intent(getActivity(),CekHasilPilkadaActivity.class));
        });

        //Agenda
        buttonAgenda.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(getActivity(),R.drawable.icon_smallagenda), null, null, null);
        buttonAgenda.setOnClickListener(view -> {
            startActivity(new Intent(getActivity(),AgendaActivity.class));
        });

        //Tata Cara
        buttonTataCara.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(getActivity(),R.drawable.icon_smallstudying), null, null, null);
        buttonTataCara.setOnClickListener(view -> {
            startActivity(new Intent(getActivity(),CaraPilkadaActivity.class));
        });

        //Pengaduan
        buttonPengaduan.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(getActivity(),R.drawable.icon_smalldemostration), null, null, null);
        buttonPengaduan.setOnClickListener(view -> {
            startActivity(new Intent(getActivity(),PengaduanActivity.class));
        });

        //Lihat Semua Berita
        viewSemuaBerita.setOnClickListener(view -> {
            startActivity(new Intent(getActivity(), BeritaPilkadaActivity.class));
        });
    }

    private void initUI() {
        // Set a Toolbar to replace the ActionBar and add Padding.
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setPadding(0,getToolBarHeight(),0,0);
//        CollapsingToolbarLayout collapsingToolbarLayout = rootView.findViewById(R.id.collapsing_toolbar);
//        collapsingToolbarLayout.setStatusBarScrim(getResources().getDrawable(R.drawable.color_gradient));
//        collapsingToolbarLayout.setContentScrim(getResources().getDrawable(R.drawable.color_gradient));

        // Remove space between ImageView and CardView
        cardViewPaslonSatu.setPreventCornerOverlap(false);
        cardViewPaslonDua.setPreventCornerOverlap(false);

        //RecyclerView
        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(new CustomDividerItemDecoration(getContext(), LinearLayoutManager.HORIZONTAL,18));
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);

        SnapHelper snapHelper = new GravitySnapHelper(Gravity.START);
        snapHelper.attachToRecyclerView(recyclerView);

        recycleBeritaAdapter = new RecycleBeritaAdapter(beritaList);
        recyclerView.setAdapter(recycleBeritaAdapter);
    }

    void initView(){
        toolbar = rootView.findViewById(R.id.toolbar);
        //Init Shimmer
        shimmerView = rootView.findViewById(R.id.view_shimer);
        //Title
        title = rootView.findViewById(R.id.title);

        //Init View Countdown
        viewOnGoing = rootView.findViewById(R.id.view_ongoing);
        viewDone = rootView.findViewById(R.id.view_done);

        //Initiate Count Down Text
        hari = rootView.findViewById(R.id.hari);
        jam = rootView.findViewById(R.id.jam);
        menit = rootView.findViewById(R.id.menit);
        detik = rootView.findViewById(R.id.detik);

        //Initiate Button Detail Paslon
        buttonDetailPaslonSatu = rootView.findViewById(R.id.btn_detailpaslonsatu);
        buttonDetailPaslonDua = rootView.findViewById(R.id.btn_detailpaslondua);

        //Initiate Button Fitur Tambahan
        buttonCekHasil = rootView.findViewById(R.id.btn_cekhasil);
        buttonAgenda = rootView.findViewById(R.id.btn_agenda);
        buttonTataCara = rootView.findViewById(R.id.btn_tatacara);
        buttonPengaduan = rootView.findViewById(R.id.btn_pengaduan);

        //Initiate View Berita Semua
        viewSemuaBerita = rootView.findViewById(R.id.view_lihatsemua);

        //Init CardView
        cardViewPaslonSatu = rootView.findViewById(R.id.paslon_1);
        cardViewPaslonDua = rootView.findViewById(R.id.paslon_2);

        //Initiate recycleview
        recyclerView = rootView.findViewById(R.id.recycle_berita);
    }

    //start timer function
    void startTimer() {
        viewOnGoing.setVisibility(View.VISIBLE);

        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy, HH:mm:ss");
        formatter.setLenient(false);

        String endTime = "27.06.2018, 07:00:00";

        Date endDate;
        try {
            endDate = formatter.parse(endTime);
            milliseconds = endDate.getTime();

        } catch (ParseException e) {
            e.printStackTrace();
        }

        startTime = System.currentTimeMillis();

        long diff = (milliseconds - startTime) - 1;

        countDownTimer = new CountDownTimer(diff, 1000) {
            public void onTick(long millisUntilFinished) {
                startTime=startTime-1;
                Long serverUptimeSeconds = millisUntilFinished / 1000;

                String daysLeft = String.format("%d", serverUptimeSeconds / 86400);
                hari.setText(daysLeft);

                String hoursLeft = String.format("%d", (serverUptimeSeconds % 86400) / 3600);
                jam.setText(hoursLeft);

                String minutesLeft = String.format("%d", ((serverUptimeSeconds % 86400) % 3600) / 60);
                menit.setText(minutesLeft);

                String secondsLeft = String.format("%d", ((serverUptimeSeconds % 86400) % 3600) % 60);
                detik.setText(secondsLeft);
            }
            public void onFinish() {
                viewOnGoing.setVisibility(View.INVISIBLE);
                title.setVisibility(View.GONE);
                viewDone.setVisibility(View.VISIBLE);

            }
        }.start();

    }

    //cancel timer
    void cancelTimer() {
        if(countDownTimer!=null)
            countDownTimer.cancel();
    }

    void getBeritaPopuler(){
        Call<List<ModelBerita>> beritaCall = ApiService.service.getBeritaPopuler();
        beritaCall.enqueue(new Callback<List<ModelBerita>>() {
            @Override
            public void onResponse(Call<List<ModelBerita>> call, Response<List<ModelBerita>> response) {
                if (response.isSuccessful()){
                    //Log.d("Successfull", String.valueOf(response.body()));
                    List<ModelBerita> modelBeritaList = response.body();

                    for (int i = 0; i < 3 ; i++) {

                        String stringTanggal = modelBeritaList.get(i).getCreated_at();
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
                        try {
                            Date dateTanggal = simpleDateFormat.parse(stringTanggal);
                            simpleDateFormat = new SimpleDateFormat("dd MMMM yyyy");
                            tanggal = simpleDateFormat.format(dateTanggal);
                            //Log.d("onFormat: ", tanggal);

                        } catch (ParseException e){
                            e.printStackTrace();
                        }

                        String stringBerita = modelBeritaList.get(i).getBody();
                        String htmlBerita = Html.fromHtml(stringBerita).toString();
                        isiBerita = Html.fromHtml(htmlBerita).toString();

                        beritaList.add(new Berita(modelBeritaList.get(i).getImg_link(), tanggal ,modelBeritaList.get(i).getTitle(), isiBerita));

                    }
                    shimmerView.stopShimmerAnimation();
                    shimmerView.setVisibility(View.GONE);
                    recycleBeritaAdapter.notifyDataSetChanged();

                } else {
                    Log.d("onResponse but Failure", String.valueOf(response.code()));
                }
            }

            @Override
            public void onFailure(Call<List<ModelBerita>> call, Throwable t) {
                Log.d("onFailure", "Something goes wrong, " + t.toString());
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        shimmerView.startShimmerAnimation();
    }

    @Override
    public void onPause() {
        shimmerView.stopShimmerAnimation();
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        cancelTimer();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        menuInflater.inflate(R.menu.menu_lain, menu);
        super.onCreateOptionsMenu(menu,menuInflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.menu_lain:
                BottomSheetDialogFragment bottomSheetDialogFragment = new ButtonSheetDialogFragment();
                bottomSheetDialogFragment.show(getChildFragmentManager(),bottomSheetDialogFragment.getTag());
        }


        return super.onOptionsItemSelected(item);
    }

    private int getToolBarHeight() {
        int height;

        Resources myResources = getResources();
        int idStatusBarHeight = myResources.getIdentifier(
                "status_bar_height", "dimen", "android");
        if (idStatusBarHeight > 0) {
            height = getResources().getDimensionPixelSize(idStatusBarHeight);
            //Toast.makeText(getActivity(), "Status Bar Height = " + height, Toast.LENGTH_LONG).show();
        }else{
            height = 0;
            //Toast.makeText(getActivity(), "Resources NOT found", Toast.LENGTH_LONG).show();
        }

        return height;
    }

}
