package com.simalakama.pilkadahubjateng.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.simalakama.pilkadahubjateng.R;
import com.simalakama.pilkadahubjateng.data.ApiService;
import com.simalakama.pilkadahubjateng.model.Response.ModelJawaban;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class JawabanSemua2Fragment extends Fragment {

    private TextView jawaban;
    private ImageView gambar;

    public JawabanSemua2Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.placeholder_container_jawaban, container, false);

        TextView paslon = rootView.findViewById(R.id.paslon);
        jawaban = rootView.findViewById(R.id.jawaban);
        gambar = rootView.findViewById(R.id.image);
        paslon.setVisibility(View.GONE);
        String id = this.getArguments().getString("id");
        getData(id);

        return rootView;
    }

    private void getData(String id){
        final Call<List<ModelJawaban>> jawabanCall = ApiService.service.getJawaban();
        jawabanCall.enqueue(new Callback<List<ModelJawaban>>() {
            @Override
            public void onResponse(Call<List<ModelJawaban>> call, Response<List<ModelJawaban>> response) {
                if (response.isSuccessful()) {
                    List<ModelJawaban> modelJawabanList = response.body();
                    for (int i = 0; i < modelJawabanList.size(); i++) {
                        if (
                                modelJawabanList.get(i).getIdPertanyaan().equals(id) &&
                                modelJawabanList.get(i).getJawabOleh().equals("paslon 2")
                                ){
                            jawaban.setText(modelJawabanList.get(i).getJawaban());
                        }
                    }
                } else {
                    Log.d("onResponse but Failure", String.valueOf(response.code()));
                }
            }

            @Override
            public void onFailure(Call<List<ModelJawaban>> call, Throwable t) {
                Log.d("onFailure", "Something goes wrong" + t.toString());
            }
        });
    }
}
