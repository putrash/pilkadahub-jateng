package com.simalakama.pilkadahubjateng.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.simalakama.pilkadahubjateng.BantuanActivity;
import com.simalakama.pilkadahubjateng.CariDpsActivity;
import com.simalakama.pilkadahubjateng.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ButtonSheetDialogFragment extends BottomSheetDialogFragment {

    LinearLayout viewBantuan, viewExit;


    public ButtonSheetDialogFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_button_sheet_dialog, container, false);

        viewBantuan = rootView.findViewById(R.id.view_bantuan);
        viewExit = rootView.findViewById(R.id.view_exit);

        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        viewBantuan.setOnClickListener(view -> {
            Intent intent = new Intent(getActivity(), BantuanActivity.class);
            startActivity(intent);
        });

        viewExit.setOnClickListener(view -> {
            getActivity().finish();
        });
    }
}
