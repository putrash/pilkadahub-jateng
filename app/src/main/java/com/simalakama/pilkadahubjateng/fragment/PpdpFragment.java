package com.simalakama.pilkadahubjateng.fragment;


import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.simalakama.pilkadahubjateng.HasilPpdpActivity;
import com.simalakama.pilkadahubjateng.R;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * A simple {@link Fragment} subclass.
 */
public class PpdpFragment extends Fragment {

    public DatabaseReference mRootReference = FirebaseDatabase.getInstance().getReference();

    String[] kabupaten = {"Pilih Kabupaten"};
    String[] kecamatan = {"Pilih Kecamatan"};
    String[] kelurahan = {"Pilih Kelurahan"};

    private ArrayList<String> kabupatenList = new ArrayList<>();
    private ArrayList<String> kecamatanList = new ArrayList<>();
    private ArrayList<String> kelurahanList = new ArrayList<>();

    private Spinner spinnerKabupaten, spinnerKecamatan, spinnerKelurahan;
    private Button buttonCari;

    String textKabupaten, textKecamatan, textKelurahan;

    public static PpdpFragment newInstance() {
        // Required empty public constructor

        return new PpdpFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_ppdp, container, false);
        setHasOptionsMenu(true);

        // Set a Toolbar to replace the ActionBar and add Padding.
        Toolbar toolbar = rootView.findViewById(R.id.toolbar);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setPadding(0,getToolBarHeight(),0,0);

        spinnerKabupaten = rootView.findViewById(R.id.spinner_kabupaten);
        spinnerKecamatan = rootView.findViewById(R.id.spinner_kecamatan);
        spinnerKelurahan = rootView.findViewById(R.id.spinner_kelurahan);

        dataKabupaten();
        spinKabupaten();
        spinKecamatan();
        spinKelurahan();

        // Initiate Button Cari
        buttonCari = rootView.findViewById(R.id.btn_cari_ppdb);

        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        buttonCari.setOnClickListener(view -> {

            if (textKabupaten == null && textKecamatan == null && textKelurahan == null ) {
                showMessageBox("Anda belum memilih salah satu daerah");
            } else if (!textKabupaten.isEmpty() && !textKecamatan.isEmpty() && !textKelurahan.isEmpty()){
                //For activity
                Intent intent = new Intent(getActivity(), HasilPpdpActivity.class);
                intent.putExtra(HasilPpdpActivity.KABUPATEN, textKabupaten.toUpperCase());
                intent.putExtra(HasilPpdpActivity.KECAMATAN, textKecamatan.toUpperCase());
                intent.putExtra(HasilPpdpActivity.KELURAHAN, textKelurahan.toUpperCase());
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            } else {
                showMessageBox("Anda belum memilih salah satu daerah");
            }
        });


    }

    void dataKabupaten() {
        kabupatenList = new ArrayList<>(Arrays.asList(kabupaten));
        mRootReference.child("daerah").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //kabupatenList.clear();
                //kabupatenList.add("Pilih Kabupaten");
                for (DataSnapshot noteDataSnapshot : dataSnapshot.getChildren()) {
                    kabupatenList.add(noteDataSnapshot.getKey());
                }
                Log.d("key", "onDataChange: " + kabupatenList);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    void spinKabupaten(){
        kabupatenList = new ArrayList<>(Arrays.asList(kabupaten));
        ArrayAdapter<String> spinnerKabupatenAdapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, kabupatenList) {

            @Override
            public boolean isEnabled(int position) {
                return position != 0;
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView textView = (TextView) view;
                if (position == 0) {
                    textView.setTextColor(getResources().getColor(R.color.colorTextSecondary));
                } else {
                    textView.setTextColor(getResources().getColor(R.color.colorTextPrimary));
                }

                return view;
            }
        };

        spinnerKabupatenAdapter.setDropDownViewResource(R.layout.spinner_item);
        spinnerKabupaten.setAdapter(spinnerKabupatenAdapter);
        spinnerKabupaten.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                if (i > 0) {
                    textKabupaten = spinnerKabupaten.getItemAtPosition(i).toString();
                    dataKecamatan();
                    spinKelurahan();
                    textKecamatan = "";
                    textKelurahan = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    void dataKecamatan() {
        kecamatanList = new ArrayList<>(Arrays.asList(kecamatan));
        mRootReference.child("daerah").child(textKabupaten).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for (DataSnapshot noteDataSnapshot : dataSnapshot.getChildren()) {

                    kecamatanList.add(noteDataSnapshot.getKey());
                }
                Log.d("key", "onDataChange: " + kecamatanList);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        spinKecamatan();
    }

    void spinKecamatan(){
        kecamatanList = new ArrayList<>(Arrays.asList(kecamatan));
        Log.d("Check", "spinKecamatan: "+kecamatanList);
        ArrayAdapter<String> spinnerKecamatanAdapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, kecamatanList) {


            @Override
            public boolean isEnabled(int position) {
                return position != 0;
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView textView = (TextView) view;
                if (position == 0) {
                    textView.setTextColor(getResources().getColor(R.color.colorTextSecondary));
                } else {
                    textView.setTextColor(getResources().getColor(R.color.colorTextPrimary));
                }

                return view;
            }
        };

        spinnerKecamatanAdapter.setDropDownViewResource(R.layout.spinner_item);
        spinnerKecamatan.setAdapter(spinnerKecamatanAdapter);
        spinnerKecamatan.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP){
                    if (kecamatanList.size() <= 1){
                        showMessageBox("Kabupaten belum dipilih");
                        return true;
                    }
                }
                return false;
            }
        });
        spinnerKecamatan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                if (i > 0) {
                    textKecamatan = spinnerKecamatan.getItemAtPosition(i).toString();
                    dataKelurahan();
                    textKelurahan = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    void dataKelurahan() {
        kelurahanList = new ArrayList<>(Arrays.asList(kelurahan));
        mRootReference.child("daerah").child(textKabupaten).child(textKecamatan).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for (DataSnapshot noteDataSnapshot : dataSnapshot.getChildren()) {
                    kelurahanList.add(noteDataSnapshot.getKey());
                }

                Log.d("key", "onDataChange: " + kelurahanList);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        spinKelurahan();
    }

    void spinKelurahan(){
        kelurahanList = new ArrayList<>(Arrays.asList(kelurahan));
        ArrayAdapter<String> spinnerKelurahanAdapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, kelurahanList) {
            @Override
            public boolean isEnabled(int position) {
                return position != 0;
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView textView = (TextView) view;
                if (position == 0) {
                    textView.setTextColor(getResources().getColor(R.color.colorTextSecondary));
                } else {
                    textView.setTextColor(getResources().getColor(R.color.colorTextPrimary));
                }

                return view;
            }
        };

        spinnerKelurahanAdapter.setDropDownViewResource(R.layout.spinner_item);
        spinnerKelurahan.setAdapter(spinnerKelurahanAdapter);
        spinnerKelurahan.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP){
                    if (kelurahanList.size() <= 1){
                        showMessageBox("Kabupaten atau Kecamatan belum dipilih");
                        return true;
                    }
                }
                return false;
            }
        });
        spinnerKelurahan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                if (i > 0) {
                    textKelurahan = spinnerKelurahan.getItemAtPosition(i).toString();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void showMessageBox(String message) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity(), R.style.customAlertDialog);
        alertDialogBuilder.setTitle("PilkadaHub");
        alertDialogBuilder.setMessage(message);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton("OK", (dialogInterface, i) -> {
            dialogInterface.dismiss();
        });
        alertDialogBuilder.show();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        menuInflater.inflate(R.menu.menu_lain, menu);
        super.onCreateOptionsMenu(menu,menuInflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.menu_lain:
                BottomSheetDialogFragment bottomSheetDialogFragment = new ButtonSheetDialogFragment();
                bottomSheetDialogFragment.show(getChildFragmentManager(),bottomSheetDialogFragment.getTag());
        }


        return super.onOptionsItemSelected(item);
    }

    private int getToolBarHeight() {
        int height;

        Resources myResources = getResources();
        int idStatusBarHeight = myResources.getIdentifier(
                "status_bar_height", "dimen", "android");
        if (idStatusBarHeight > 0) {
            height = getResources().getDimensionPixelSize(idStatusBarHeight);
            //Toast.makeText(getActivity(), "Status Bar Height = " + height, Toast.LENGTH_LONG).show();
        }else{
            height = 0;
            //Toast.makeText(getActivity(), "Resources NOT found", Toast.LENGTH_LONG).show();
        }

        return height;
    }

}
