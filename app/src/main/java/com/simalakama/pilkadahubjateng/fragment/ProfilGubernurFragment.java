package com.simalakama.pilkadahubjateng.fragment;



import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.simalakama.pilkadahubjateng.R;
import com.simalakama.pilkadahubjateng.adapter.ListDetailProfilAdapter;
import com.simalakama.pilkadahubjateng.data.ApiService;
import com.simalakama.pilkadahubjateng.model.Response.ModelPaslon;
import com.squareup.picasso.Picasso;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfilGubernurFragment extends Fragment {

    String[] textTitle = {"Jenis Kelamin","Tempat Lahir", "Tanggal Lahir",
            "Pekerjaan Kepala Daerah","Jumlah Dukungan", "Status Petahana","Partai Pendukung"};

    String[] textIsi = {"","", "","","0", "",""};

    private String id;
    private ListView listView;
    private TextView textNamaCagub;
    private int profile;
    private View headerView;

    public ProfilGubernurFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_profil_gubernur, container, false);



        id =  getArguments().getString("ID");
        if (id.equals("1")){
            id = "5ad04fccf637c50414aced74";
            profile = R.drawable.icon_gubernursatu;
        } else if (id.equals("2")){
            id = "5ad04fccf637c50414aced75";
            profile = R.drawable.icon_gubernurdua;
        }

        listView = rootView.findViewById(R.id.list_detail_profil);
        headerView = inflater.inflate(R.layout.header_list_profilgubernur,listView,false);

        getDetailPaslon();

        // Init UI
        ImageButton buttonTwitter = headerView.findViewById(R.id.btn_twitter);
        buttonTwitter.setImageResource(R.drawable.icon_twitter);

        ImageButton butonFacebook = headerView.findViewById(R.id.btn_facebook);
        butonFacebook.setImageResource(R.drawable.icon_facebook);

        ImageButton buttonInstagram = headerView.findViewById(R.id.btn_instagram);
        buttonInstagram.setImageResource(R.drawable.icon_instagram);

        textNamaCagub = headerView.findViewById(R.id.text_namacagub);

        ImageView imageProfile = headerView.findViewById(R.id.image_profilegubernur);
        Picasso.get()
                .load(profile)
                .into(imageProfile);
        imageProfile.setScaleType(ImageView.ScaleType.CENTER_CROP);

        return rootView;
    }

    void getDetailPaslon(){
        Call<ModelPaslon> paslonCall = ApiService.service.getDetailPaslon(id);
        paslonCall.enqueue(new Callback<ModelPaslon>() {
            @Override
            public void onResponse(Call<ModelPaslon> call, Response<ModelPaslon> response) {
                if (response.isSuccessful()){
                    Log.d("Successfull", String.valueOf(response.body()));

                    ModelPaslon modelPaslon = response.body();
                    textNamaCagub.setText(modelPaslon.getNamaKepalaDaerah());
                    textIsi[0] = modelPaslon.getGenderKepalaDaerah();
                    textIsi[1] = modelPaslon.getTempatLahirKepalaDaerah();
                    textIsi[2] = modelPaslon.getTanggalLahirKepalaDaerah();
                    textIsi[3] = modelPaslon.getPekerjaanKepalaDaerah();
                    textIsi[4] = modelPaslon.getJumlahDukungan();
                    textIsi[5] = modelPaslon.getStatusPertahana();
                    textIsi[6] = modelPaslon.getPartaiPendukung();

                    listView.setAdapter(new ListDetailProfilAdapter(getActivity(),textTitle,textIsi));
                    listView.addHeaderView(headerView);
//                    getListView(listView);


                } else {
                    Log.d("onResponse but Failure", String.valueOf(response.code()));
                }
            }

            @Override
            public void onFailure(Call<ModelPaslon> call, Throwable t) {
                Log.d("onFailure", "Something goes wrong" + t.toString());
            }
        });
    }

}
