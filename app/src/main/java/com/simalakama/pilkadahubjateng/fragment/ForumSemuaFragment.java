package com.simalakama.pilkadahubjateng.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.simalakama.pilkadahubjateng.R;
import com.simalakama.pilkadahubjateng.adapter.ListForumAdapter;
import com.simalakama.pilkadahubjateng.data.ApiService;
import com.simalakama.pilkadahubjateng.model.Forum;
import com.simalakama.pilkadahubjateng.model.Response.ModelPertanyaan;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForumSemuaFragment extends Fragment {
    private List<Forum> forumList = new ArrayList<>();
    private ListForumAdapter listAdapter;
    private ShimmerFrameLayout shimmerView;
    private ListView listView;
    private String txHead;
    private TextView headText;
    private Call<List<ModelPertanyaan>> pertanyaanCall = ApiService.service.getPertanyaan();

    public ForumSemuaFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_forum_list, container, false);

        shimmerView = rootView.findViewById(R.id.view_shimer);
        listView = rootView.findViewById(R.id.list_forum);

        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getForumSemua();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        pertanyaanCall.cancel();
    }

    void getForumSemua() {
        pertanyaanCall.clone().enqueue(new Callback<List<ModelPertanyaan>>() {
            @Override
            public void onResponse(Call<List<ModelPertanyaan>> call, Response<List<ModelPertanyaan>> response) {
                if (response.isSuccessful()) {
                    forumList.clear();
                    List<ModelPertanyaan> modelPertanyaanList = response.body();
                    for (int i = 0; i < modelPertanyaanList.size(); i++) {
                        String status;
                        if (modelPertanyaanList.get(i).getStatus() == null) {
                            status = "0";
                        } else {
                            status = modelPertanyaanList.get(i).getStatus();
                        }
                        if (status.equals("2")) {
                            if (modelPertanyaanList.get(i).getTanyaKe().equals("Semua")) {
                                String pertanyaan = modelPertanyaanList.get(i).getPertanyaan();
                                String nama = modelPertanyaanList.get(i).getNama();
                                String tanggal = modelPertanyaanList.get(i).getTanggal();
                                String id = modelPertanyaanList.get(i).getId();
                                String tanyaKe = modelPertanyaanList.get(i).getTanyaKe();
                                forumList.add(new Forum(id, pertanyaan, tanyaKe, nama, tanggal));
                            }
                        }
                    }
                    if (isAdded()) {
                        if (forumList.size() == 0) {
                            txHead = "Pertanyaan yang ditujukan untuk paslon 1 dan paslon 2 belum ada. Jadilah yang pertama memberikan pertanyaan !";
                        } else {
                            txHead = "Terdapat kumpulan pertanyaan yang ditujukan untuk paslon 1 dan paslon 2";
                        }
                        listAdapter = new ListForumAdapter(getActivity(), forumList);
                        listView.setAdapter(listAdapter);
                        View headerList = getLayoutInflater().inflate(R.layout.header_list_forum, listView, false);
                        headerList.setClickable(false);
                        headerList.setBackgroundColor(getResources().getColor(R.color.colorTransparent));
                        headText = headerList.findViewById(R.id.headText);
                        headText.setText(txHead);
                        listView.addHeaderView(headerList);
                        shimmerView.stopShimmerAnimation();
                        shimmerView.setVisibility(View.GONE);
                    }
                } else {
                    Log.d("onResponse but Failure", String.valueOf(response.code()));
                }
            }

            @Override
            public void onFailure(Call<List<ModelPertanyaan>> call, Throwable t) {
                Log.d("onFailure", "Something goes wrong" + t.toString());
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        shimmerView.startShimmerAnimation();
    }

    @Override
    public void onPause() {
        shimmerView.stopShimmerAnimation();
        super.onPause();
    }
}
