package com.simalakama.pilkadahubjateng;

import android.content.Intent;
import android.content.res.Resources;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.widget.ImageView;

import com.github.rubensousa.gravitysnaphelper.GravitySnapHelper;
import com.simalakama.pilkadahubjateng.adapter.RecycleBeritaLainnyaAdapter;
import com.simalakama.pilkadahubjateng.adapter.RecycleVideoBeritaAdapter;
import com.simalakama.pilkadahubjateng.custom.CustomDividerItemDecoration;
import com.simalakama.pilkadahubjateng.data.ApiService;
import com.simalakama.pilkadahubjateng.model.Berita;
import com.simalakama.pilkadahubjateng.model.Response.ModelVideo;
import com.simalakama.pilkadahubjateng.model.Video;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VideoActivity extends AppCompatActivity {

    private List<Video> videoList = new ArrayList<>();
    private RecyclerView recyclerViewVideo;

    private RecycleVideoBeritaAdapter recycleVideoAdapter;
    private SwipeRefreshLayout swipeRefresh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);

        // Set a Toolbar to replace the ActionBar and add Padding.
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setPadding(0,getToolBarHeight(),0,0);

        //Initiate RecycleView Berita Lainnya
        recyclerViewVideo = findViewById(R.id.recycle_videoberita);
        recyclerViewVideo.setHasFixedSize(true);
        recyclerViewVideo.setNestedScrollingEnabled(false);

        RecyclerView.LayoutManager layoutManagerBerita = new LinearLayoutManager(getApplicationContext());
        recyclerViewVideo.setLayoutManager(layoutManagerBerita);

        SnapHelper snapHelperTop = new GravitySnapHelper(Gravity.TOP);
        snapHelperTop.attachToRecyclerView(recyclerViewVideo);

        recycleVideoAdapter = new RecycleVideoBeritaAdapter(videoList);
        recyclerViewVideo.setAdapter(recycleVideoAdapter);

//        ImageView imageExample = findViewById(R.id.example);
//        imageExample.setOnClickListener(view -> {
//            startActivity(new Intent(this, VideoFullscreenActivity.class));
//        });

        //Get Data
        getVideo();

        swipeRefresh = findViewById(R.id.swipe_refresh);
        swipeRefresh.setColorSchemeColors(getResources().getColor(R.color.colorPrimaryDark));
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshItem();
            }

            void refreshItem(){
                getVideo();
                onItemLoad();
            }

            void onItemLoad(){
                swipeRefresh.setRefreshing(false);
            }
        });
    }

    void getVideo(){
        Call<List<ModelVideo>> videoCall = ApiService.service.getVideo();
        videoCall.enqueue(new Callback<List<ModelVideo>>() {
            @Override
            public void onResponse(Call<List<ModelVideo>> call, Response<List<ModelVideo>> response) {
                if (response.isSuccessful()){
                    //Log.d("Successfull", String.valueOf(response.body()));
                    List<ModelVideo> modelVideoList = response.body();
                    recycleVideoAdapter.clear();

                    for (int i = 0; i < modelVideoList.size() ; i++) {

                        videoList.add(new Video(modelVideoList.get(i).getJudul(),modelVideoList.get(i).getDurasi(),modelVideoList.get(i).getThumbnail(),modelVideoList.get(i).getVideo()));
                    }

                    recycleVideoAdapter.notifyDataSetChanged();

                } else {
                    Log.d("onResponse but Failure", String.valueOf(response.code()));
                }
            }

            @Override
            public void onFailure(Call<List<ModelVideo>> call, Throwable t) {
                Log.d("onFailure", "Something goes wrong" + t.toString());
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private int getToolBarHeight() {
        int height;

        Resources myResources = getResources();
        int idStatusBarHeight = myResources.getIdentifier(
                "status_bar_height", "dimen", "android");
        if (idStatusBarHeight > 0) {
            height = getResources().getDimensionPixelSize(idStatusBarHeight);
            //Toast.makeText(getActivity(), "Status Bar Height = " + height, Toast.LENGTH_LONG).show();
        }else{
            height = 0;
            //Toast.makeText(getActivity(), "Resources NOT found", Toast.LENGTH_LONG).show();
        }

        return height;
    }


}
