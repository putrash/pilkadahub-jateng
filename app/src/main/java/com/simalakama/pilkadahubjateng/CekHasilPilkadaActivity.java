package com.simalakama.pilkadahubjateng;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Arrays;

public class CekHasilPilkadaActivity extends AppCompatActivity {

    public DatabaseReference mRootReference = FirebaseDatabase.getInstance().getReference();

    private String[] kabupaten = {"Pilih Kabupaten"};
    private String[] kecamatan = {"Pilih Kecamatan"};
    private String[] kelurahan = {"Pilih Kelurahan"};
    private String[] tps = {"Pilih TPS"};

    private ArrayList<String> kabupatenList = new ArrayList<>();
    private ArrayList<String> kecamatanList = new ArrayList<>();
    private ArrayList<String> kelurahanList = new ArrayList<>();
    private ArrayList<String> tpsList = new ArrayList<>();

    private Spinner spinnerKabupaten, spinnerKecamatan, spinnerKelurahan, spinnerTps;
    private String textKabupaten, textKecamatan, textKelurahan, textTPS;
    private int daerah = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cekhasil_pilkada);

        // Set a Toolbar to replace the ActionBar and add Padding.
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setPadding(0, getToolBarHeight(), 0, 0);

        Button buttonCek = findViewById(R.id.btn_cekhasil);

        spinnerKabupaten = findViewById(R.id.spinner_kabupaten);
        spinnerKecamatan = findViewById(R.id.spinner_kecamatan);
        spinnerKelurahan = findViewById(R.id.spinner_kelurahan);
        spinnerTps = findViewById(R.id.spinner_tps);

        dataKabupaten();
        spinKabupaten();
        spinKecamatan();
        spinKelurahan();
        spinTps();

        buttonCek.setOnClickListener(view -> {
            DialogInterface.OnClickListener dialogClickListener = (dialog, which) -> {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        Intent intent = new Intent(CekHasilPilkadaActivity.this, HasilPilkadaActivity.class);
                        intent.putExtra("kabupaten", textKabupaten);
                        intent.putExtra("kecamatan", textKecamatan);
                        intent.putExtra("kelurahan", textKelurahan);
                        intent.putExtra("tps", textTPS);
                        intent.putExtra("daerah", daerah);
                        startActivity(intent);
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        dialog.dismiss();
                        break;
                }
            };

            if (textKabupaten.isEmpty() && textKecamatan.isEmpty() && textKelurahan.isEmpty() && textTPS.isEmpty()) {
                showMessageBox("Anda belum memilih salah satu daerah");
            } else if (!textKabupaten.isEmpty() && textKecamatan.isEmpty() && textKelurahan.isEmpty() && textTPS.isEmpty()){
                daerah = 1;
                AlertDialog.Builder builder = new AlertDialog.Builder(CekHasilPilkadaActivity.this);
                builder.setMessage("Apakah anda yakin melihat hasil pilkada di Kabupaten "+textKabupaten+"?").setPositiveButton("Ya", dialogClickListener)
                        .setNegativeButton("tidak", dialogClickListener).show();
            } else if (!textKabupaten.isEmpty() && !textKecamatan.isEmpty() && textKelurahan.isEmpty() && textTPS.isEmpty()) {
                daerah = 2;
                AlertDialog.Builder builder = new AlertDialog.Builder(CekHasilPilkadaActivity.this);
                builder.setMessage("Apakah anda yakin melihat hasil pilkada di Kecamatan "+textKecamatan+"?").setPositiveButton("Ya", dialogClickListener)
                        .setNegativeButton("tidak", dialogClickListener).show();
            } else if (!textKabupaten.isEmpty() && !textKecamatan.isEmpty() && !textKelurahan.isEmpty() && textTPS.isEmpty()) {
                daerah = 3;
                AlertDialog.Builder builder = new AlertDialog.Builder(CekHasilPilkadaActivity.this);
                builder.setMessage("Apakah anda yakin melihat hasil pilkada di Kelurahan "+textKelurahan+"?").setPositiveButton("Ya", dialogClickListener)
                        .setNegativeButton("tidak", dialogClickListener).show();
            } else if (!textKabupaten.isEmpty() && !textKecamatan.isEmpty() && !textKelurahan.isEmpty() && !textTPS.isEmpty()) {
                daerah = 4;
                AlertDialog.Builder builder = new AlertDialog.Builder(CekHasilPilkadaActivity.this);
                builder.setMessage("Apakah anda yakin melihat hasil pilkada di TPS "+textKelurahan+"?").setPositiveButton("Ya", dialogClickListener)
                        .setNegativeButton("tidak", dialogClickListener).show();
            } else {
                showMessageBox("Anda belum memilih salah satu daerah");
            }

        });
    }

    void dataKabupaten() {
        kabupatenList = new ArrayList<>(Arrays.asList(kabupaten));
        mRootReference.child("daerah").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //kabupatenList.clear();
                //kabupatenList.add("Pilih Kabupaten");
                for (DataSnapshot noteDataSnapshot : dataSnapshot.getChildren()) {
                    kabupatenList.add(noteDataSnapshot.getKey());
                }
                Log.d("key", "onDataChange: " + kabupatenList);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    void spinKabupaten(){
        kabupatenList = new ArrayList<>(Arrays.asList(kabupaten));
        ArrayAdapter<String> spinnerKabupatenAdapter = new ArrayAdapter<String>(CekHasilPilkadaActivity.this, R.layout.spinner_item, kabupatenList) {

            @Override
            public boolean isEnabled(int position) {
                return position != 0;
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView textView = (TextView) view;
                if (position == 0) {
                    textView.setTextColor(getResources().getColor(R.color.colorTextSecondary));
                } else {
                    textView.setTextColor(getResources().getColor(R.color.colorTextPrimary));
                }

                return view;
            }
        };

        spinnerKabupatenAdapter.setDropDownViewResource(R.layout.spinner_item);
        spinnerKabupaten.setAdapter(spinnerKabupatenAdapter);
        spinnerKabupaten.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                if (i > 0) {
                    textKabupaten = spinnerKabupaten.getItemAtPosition(i).toString();
                    dataKecamatan();
                    spinKelurahan();
                    spinTps();
                    textKecamatan = "";
                    textKelurahan = "";
                    textTPS = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    void dataKecamatan() {
        kecamatanList = new ArrayList<>(Arrays.asList(kecamatan));
        mRootReference.child("daerah").child(textKabupaten).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for (DataSnapshot noteDataSnapshot : dataSnapshot.getChildren()) {

                    kecamatanList.add(noteDataSnapshot.getKey());
                }
                Log.d("key", "onDataChange: " + kecamatanList);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        spinKecamatan();
    }

    void spinKecamatan(){
        kecamatanList = new ArrayList<>(Arrays.asList(kecamatan));
        Log.d("Check", "spinKecamatan: "+kecamatanList);
        ArrayAdapter<String> spinnerKecamatanAdapter = new ArrayAdapter<String>(CekHasilPilkadaActivity.this, R.layout.spinner_item, kecamatanList) {


            @Override
            public boolean isEnabled(int position) {
                return position != 0;
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView textView = (TextView) view;
                if (position == 0) {
                    textView.setTextColor(getResources().getColor(R.color.colorTextSecondary));
                } else {
                    textView.setTextColor(getResources().getColor(R.color.colorTextPrimary));
                }

                return view;
            }
        };

        spinnerKecamatanAdapter.setDropDownViewResource(R.layout.spinner_item);
        spinnerKecamatan.setAdapter(spinnerKecamatanAdapter);
        spinnerKecamatan.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP){
                    if (kecamatanList.size() <= 1){
                        showMessageBox("Kabupaten belum dipilih");
                        return true;
                    }
                }
                return false;
            }
        });
        spinnerKecamatan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                if (i > 0) {
                    textKecamatan = spinnerKecamatan.getItemAtPosition(i).toString();
                    dataKelurahan();
                    spinTps();
                    textKelurahan = "";
                    textTPS = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    void dataKelurahan() {
        kelurahanList = new ArrayList<>(Arrays.asList(kelurahan));
        mRootReference.child("daerah").child(textKabupaten).child(textKecamatan).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for (DataSnapshot noteDataSnapshot : dataSnapshot.getChildren()) {
                    kelurahanList.add(noteDataSnapshot.getKey());
                }

                Log.d("key", "onDataChange: " + kelurahanList);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        spinKelurahan();
    }

    void spinKelurahan(){
        kelurahanList = new ArrayList<>(Arrays.asList(kelurahan));
        ArrayAdapter<String> spinnerKelurahanAdapter = new ArrayAdapter<String>(CekHasilPilkadaActivity.this, R.layout.spinner_item, kelurahanList) {
            @Override
            public boolean isEnabled(int position) {
                return position != 0;
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView textView = (TextView) view;
                if (position == 0) {
                    textView.setTextColor(getResources().getColor(R.color.colorTextSecondary));
                } else {
                    textView.setTextColor(getResources().getColor(R.color.colorTextPrimary));
                }

                return view;
            }
        };

        spinnerKelurahanAdapter.setDropDownViewResource(R.layout.spinner_item);
        spinnerKelurahan.setAdapter(spinnerKelurahanAdapter);
        spinnerKelurahan.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP){
                    if (kelurahanList.size() <= 1){
                        showMessageBox("Kabupaten atau Kecamatan belum dipilih");
                        return true;
                    }
                }
                return false;
            }
        });
        spinnerKelurahan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                if (i > 0) {
                    textKelurahan = spinnerKelurahan.getItemAtPosition(i).toString();
                    dataTPS();
                    textTPS = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void dataTPS() {
        tpsList = new ArrayList<>(Arrays.asList(tps));
        mRootReference.child("daerah").child(textKabupaten).child(textKecamatan).child(textKelurahan).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for (DataSnapshot noteDataSnapshot : dataSnapshot.getChildren()) {
                    tpsList.add(noteDataSnapshot.getKey());
                    //Log.d("tes", "onDataChange: " + noteDataSnapshot.getValue().toString());
                }

                Log.d("key", "onDataChange: " + tpsList);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        spinTps();
    }

    private void spinTps() {
        tpsList = new ArrayList<>(Arrays.asList(tps));
        ArrayAdapter<String> spinnerTpsAdapter = new ArrayAdapter<String>(CekHasilPilkadaActivity.this, R.layout.spinner_item, tpsList) {
            @Override
            public boolean isEnabled(int position) {
                return position != 0;
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView textView = (TextView) view;
                if (position == 0) {
                    textView.setTextColor(getResources().getColor(R.color.colorTextSecondary));
                } else {
                    textView.setTextColor(getResources().getColor(R.color.colorTextPrimary));
                }

                return view;
            }
        };

        spinnerTpsAdapter.setDropDownViewResource(R.layout.spinner_item);
        spinnerTps.setAdapter(spinnerTpsAdapter);
        spinnerTps.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP){
                    if (tpsList.size() <= 1){
                        showMessageBox("Kabupaten/Kecamatan/Kelurahan belum dipilih");
                        return true;
                    }
                }
                return false;
            }
        });
        spinnerTps.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                if (i > 0) {
                    textTPS = spinnerTps.getItemAtPosition(i).toString();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void showMessageBox(String message) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this, R.style.customAlertDialog);
        alertDialogBuilder.setTitle("PilkadaHub");
        alertDialogBuilder.setMessage(message);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton("OK", (dialogInterface, i) -> {
            dialogInterface.dismiss();
        });
        alertDialogBuilder.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private int getToolBarHeight() {
        int height;

        Resources myResources = getResources();
        int idStatusBarHeight = myResources.getIdentifier(
                "status_bar_height", "dimen", "android");
        if (idStatusBarHeight > 0) {
            height = getResources().getDimensionPixelSize(idStatusBarHeight);
            //Toast.makeText(this, "Status Bar Height = " + height, Toast.LENGTH_LONG).show();
        } else {
            height = 0;
            //Toast.makeText(getActivity(), "Resources NOT found", Toast.LENGTH_LONG).show();
        }

        return height;
    }
}
