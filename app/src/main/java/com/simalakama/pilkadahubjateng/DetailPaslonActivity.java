package com.simalakama.pilkadahubjateng;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.simalakama.pilkadahubjateng.adapter.GridDetailPaslonAdapter;
import com.simalakama.pilkadahubjateng.data.ApiService;
import com.simalakama.pilkadahubjateng.fragment.ForumFragment;
import com.simalakama.pilkadahubjateng.model.HasilPpdb;
import com.simalakama.pilkadahubjateng.model.Response.ModelAgenda;
import com.simalakama.pilkadahubjateng.model.Response.ModelPaslon;

import java.util.ArrayList;
import java.util.List;

import in.srain.cube.views.GridViewWithHeaderAndFooter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailPaslonActivity extends AppCompatActivity {

    public static String ID_PASLON;
    private String namaPaslon, nomorUrut, textMotto;

    String[] textMenu = {"Profil Paslon", "Dana Paslon",
            "Visi Misi", "Program Kerja", "Janji - Janji", "Prestasi"};

    int[] imageIcon = {R.drawable.icon_boss, R.drawable.icon_money,
            R.drawable.icon_target, R.drawable.icon_presentation , R.drawable.icon_clipboard, R.drawable.icon_award};
    private int image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_paslon);

        String id = getIntent().getStringExtra(ID_PASLON);
        switch (id) {
            case "1":
                namaPaslon = "GANJAR - YASIN";
                nomorUrut = "1";
                textMotto = "Mboten Korupsi, Mboten Ngapusi";
                image = R.drawable.img_paslon_pertama;
                break;
            case "2":
                namaPaslon = "SUDIRMAN - IDA";
                nomorUrut = "2";
                textMotto = "Mbangun Jateng, Mukti Bareng";
                image = R.drawable.img_paslon_kedua;
                break;

        }
        // Set a Toolbar to replace the ActionBar and add Padding.
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        CollapsingToolbarLayout collapsingToolbarLayout = findViewById(R.id.collapsing_toolbar);
        collapsingToolbarLayout.setStatusBarScrim(getResources().getDrawable(R.drawable.color_gradient));
        collapsingToolbarLayout.setContentScrim(getResources().getDrawable(R.drawable.color_gradient));

        // Initiate GridView
        GridViewWithHeaderAndFooter gridView = findViewById(R.id.grid_detailpaslon);
        View headerView = getLayoutInflater().inflate(R.layout.header_grid_detailpaslon, gridView, false);
        headerView.setClickable(false);
        headerView.setBackgroundColor(getResources().getColor(R.color.colorTransparent));

        View footerView = getLayoutInflater().inflate(R.layout.footer_grid_detailpaslon, gridView, false);
        footerView.setClickable(false);
        footerView.setBackgroundColor(getResources().getColor(R.color.colorTransparent));

        // Initiate UI
        ImageView imageViewPaslon = findViewById(R.id.image_paslon);
        imageViewPaslon.setImageResource(image);
        TextView textViewNama = headerView.findViewById(R.id.text_namapaslon);
        textViewNama.setText(namaPaslon);
        TextView textViewNo = headerView.findViewById(R.id.text_nourut);
        textViewNo.setText(nomorUrut);
        TextView textViewMotto = headerView.findViewById(R.id.text_motto);
        textViewMotto.setText(textMotto);

        Button btnTanya = footerView.findViewById(R.id.btn_tanya);
        btnTanya.setOnClickListener(view -> {
            Intent intent = new Intent(this, TambahPertanyaanActivity.class);
            if (id.equals("1")) {
                intent.putExtra("PASLON",2);
            } else if (id.equals("2")) {
                intent.putExtra("PASLON",3);
            }
            startActivity(intent);

        });

        gridView.addHeaderView(headerView);
        gridView.addFooterView(footerView);
        gridView.setAdapter(new GridDetailPaslonAdapter(this, textMenu, imageIcon));
        gridView.setOnItemClickListener((adapterView, view, i, l) -> {
            Class selectedClass = null;
            // TODO: 4/13/2018 GANTI CLASS UNTUK SETIAP CASE
            switch (i) {

                case 0:
                    //Profil Paslon
                    selectedClass = ProfilPaslonActivity.class;
                    break;
                case 1:
                    //Dana Paslon
                    selectedClass = DanaPaslonActivity.class;
                    break;
                case 2:
                    //Visi Misi
                    selectedClass = VisiMisiActivity.class;
                    break;
                case 3:
                    //Program Kerja
                    selectedClass = ProgramKerjaActivity.class;
                    break;
                case 4:
                    //Janji - Janji
                    selectedClass = JanjiActivity.class;
                    break;
                case 5:
                    //Prestasi
                    selectedClass = PrestasiActivity.class;
                    break;
            }

            Intent intent = new Intent(DetailPaslonActivity.this, selectedClass);
            if (id.equals("1")) {
                //Log.d("ID", "onItemClick: "+ id );
                intent.putExtra("ID", "1");
            } else if (id.equals("2")) {
                //Log.d("ID", "onItemClick: " + id);
                intent.putExtra("ID", "2");
            }
            startActivity(intent);
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
