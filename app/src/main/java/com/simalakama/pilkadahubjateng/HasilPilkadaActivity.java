package com.simalakama.pilkadahubjateng;

import android.app.Dialog;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.daimajia.numberprogressbar.NumberProgressBar;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

public class HasilPilkadaActivity extends AppCompatActivity {

    public DatabaseReference mRootReference = FirebaseDatabase.getInstance().getReference();

    private TextView textLokasi, textHeader, textSubTitle;
    private Button btnLapor;
    private TextView suara1, suara2, suara3, suara4, jml_pemilih;
    private NumberProgressBar progressBar1, progressBar2, progressBar3;
    private ImageView imageView;
    private Bitmap imgBitmap;

    private String kabupaten, kecamatan, kelurahan, tps;
    private int daerah, s1 = 0, s2 = 0, s3 = 0, s4 = 0, s5 = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hasil_pilkada);

        kabupaten = getIntent().getStringExtra("kabupaten");
        kecamatan = getIntent().getStringExtra("kecamatan");
        kelurahan = getIntent().getStringExtra("kelurahan");
        tps = getIntent().getStringExtra("tps");
        daerah = getIntent().getIntExtra("daerah", 1);

        // Set a Toolbar to replace the ActionBar and add Padding.
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setPadding(0, getToolBarHeight(), 0, 0);

        textLokasi = findViewById(R.id.text_lokasi);
        textHeader = findViewById(R.id.text_headertitle);
        textSubTitle = findViewById(R.id.text_subtitle);

        suara1 = findViewById(R.id.suara_1);
        suara2 = findViewById(R.id.suara_2);
        suara3 = findViewById(R.id.suara_3);
        suara4 = findViewById(R.id.jumlah_golput);
        jml_pemilih = findViewById(R.id.jumlah_pemilih);

        progressBar1 = findViewById(R.id.progressbar1);
        progressBar2 = findViewById(R.id.progressbar2);
        progressBar3 = findViewById(R.id.progressbar3);
        imageView = findViewById(R.id.image_view);

        btnLapor = findViewById(R.id.btn_lapor);
        btnLapor.setOnClickListener(view -> {
            Intent intent = new Intent(HasilPilkadaActivity.this, PengaduanActivity.class);
            startActivity(intent);
        });
        getData();

        imageView.setOnClickListener(v -> {
            Dialog modal = new Dialog(HasilPilkadaActivity.this);
            modal.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            modal.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

            LayoutInflater inflater = getLayoutInflater();
            View newView = inflater.inflate(R.layout.image_modal, null);
            modal.setContentView(newView);

            ImageView iv = newView.findViewById(R.id.img);
            LinearLayout ll = newView.findViewById(R.id.layout);
            iv.setImageBitmap(imgBitmap);
            iv.setOnClickListener(v1 -> modal.dismiss());
            ll.setOnClickListener(v1 -> modal.dismiss());
            modal.show();
        });
    }

    private void getData() {
        if (daerah == 1) {
            textLokasi.setText("Kabupaten " + kabupaten);
            textHeader.append("Kabupaten " + kabupaten);
            textSubTitle.append("Kabupaten " + kabupaten);

            mRootReference.child("daerah").child(kabupaten).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot kecamatan : dataSnapshot.getChildren()) {
                        for (DataSnapshot kelurahan : kecamatan.getChildren()) {
                            for (DataSnapshot tps : kelurahan.getChildren()) {
                                mRootReference
                                        .child("vote")
                                        .orderByChild("id_daerah")
                                        .equalTo(tps.getValue().toString())
                                        .limitToFirst(1).
                                        addListenerForSingleValueEvent(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(DataSnapshot dataSnap) {
                                                prosesData(dataSnap);
                                            }

                                            @Override
                                            public void onCancelled(DatabaseError databaseError) {

                                            }
                                        });
                            }
                        }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        } else if (daerah == 2) {
            textLokasi.setText("Kecamatan " + kecamatan);
            textHeader.append("Kecamatan " + kecamatan);
            textSubTitle.append("Kecamatan " + kecamatan);
            mRootReference.child("daerah").child(kabupaten + "/" + kecamatan).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot kelurahan : dataSnapshot.getChildren()) {
                        for (DataSnapshot tps : kelurahan.getChildren()) {
                            mRootReference
                                    .child("vote")
                                    .orderByChild("id_daerah")
                                    .equalTo(tps.getValue().toString())
                                    .limitToFirst(1).
                                    addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnap) {
                                            prosesData(dataSnap);
                                        }

                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {

                                        }
                                    });
                        }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        } else if (daerah == 3) {
            textLokasi.setText("Kelurahan " + kelurahan);
            textHeader.append("Kelurahan " + kelurahan);
            textSubTitle.append("Kelurahan " + kelurahan);
            mRootReference.child("daerah").child(kabupaten + "/" + kecamatan + "/" + kelurahan).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot tps : dataSnapshot.getChildren()) {
                        mRootReference
                                .child("vote")
                                .orderByChild("id_daerah")
                                .equalTo(tps.getValue().toString())
                                .limitToFirst(1).
                                addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnap) {
                                        prosesData(dataSnap);
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {

                                    }
                                });
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        } else if (daerah == 4) {
            textLokasi.setText("Kelurahan " + kelurahan);
            textHeader.append("Kelurahan " + kelurahan);
            String noTps = " " + tps.substring(0, 3).toUpperCase() + " " + tps.substring(3);
            textSubTitle.append(noTps);
            mRootReference.child("daerah").child(kabupaten + "/" + kecamatan + "/" + kelurahan + "/" + tps).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    mRootReference
                            .child("vote")
                            .orderByChild("id_daerah")
                            .equalTo(dataSnapshot.getValue().toString())
                            .limitToFirst(1).
                            addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnap) {
                                    prosesData(dataSnap);
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private int getToolBarHeight() {
        int height;

        Resources myResources = getResources();
        int idStatusBarHeight = myResources.getIdentifier(
                "status_bar_height", "dimen", "android");
        if (idStatusBarHeight > 0) {
            height = getResources().getDimensionPixelSize(idStatusBarHeight);
            //Toast.makeText(this, "Status Bar Height = " + height, Toast.LENGTH_LONG).show();
        } else {
            height = 0;
            //Toast.makeText(getActivity(), "Resources NOT found", Toast.LENGTH_LONG).show();
        }

        return height;
    }

    private void prosesData(DataSnapshot tps) {
        if (tps.getChildrenCount() > 0) {
            for (DataSnapshot noteDataSnapshot : tps.getChildren()) {
                s1 = Integer.parseInt(noteDataSnapshot.child("paslon").child("id1").getValue().toString());
                s2 = Integer.parseInt(noteDataSnapshot.child("paslon").child("id2").getValue().toString());
                s3 = Integer.parseInt(noteDataSnapshot.child("tidak_sah").getValue().toString());
                s5 = Integer.parseInt(noteDataSnapshot.child("jumlah_dps").getValue().toString());
                s4 = s5 - (s1 + s2 + s3);

                suara1.setText(String.valueOf(s1));
                suara2.setText(String.valueOf(s2));
                suara3.setText(String.valueOf(s3));
                suara4.setText(String.valueOf(s4));
                jml_pemilih.setText(String.valueOf(s5));

                progressBar1.setMax(s5);
                progressBar2.setMax(s5);
                progressBar3.setMax(s5);
                progressBar1.setProgress(s1);
                progressBar2.setProgress(s2);
                progressBar3.setProgress(s3);
                if (daerah == 4) {
                    String gambar = noteDataSnapshot.child("bukti").getValue().toString();
                    Glide.with(HasilPilkadaActivity.this)
                            .load("https://firebasestorage.googleapis.com/v0/b/pilkadahub.appspot.com/o/bukti%2F" + noteDataSnapshot.getKey() + "%2F" + gambar + "?alt=media")
                            .asBitmap()
                            .into(new SimpleTarget<Bitmap>() {
                                @Override
                                public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                                    imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                                    imageView.setImageBitmap(resource);
                                    imgBitmap = resource;
                                }
                            });
                }
            }
        }
    }
}
