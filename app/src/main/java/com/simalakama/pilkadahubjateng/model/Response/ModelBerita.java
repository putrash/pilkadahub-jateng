package com.simalakama.pilkadahubjateng.model.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Putraa on 4/19/2018.
 */

public class ModelBerita {
    @SerializedName("created_at")
    @Expose
    private String created_at;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("body")
    @Expose
    private String body;
    @SerializedName("tag")
    @Expose
    private String tag;
    @SerializedName("img_link")
    @Expose
    private String img_link;
    @SerializedName("visitor")
    @Expose
    private String visitor;

    public ModelBerita(String created_at, String title, String body, String tag, String img_link, String visitor){
        this.created_at = created_at;
        this.title = title;
        this.body = body;
        this.tag = tag;
        this.img_link = img_link;
        this.visitor = visitor;
    }

    public String getCreated_at(){
        return created_at;
    }

    public String getTitle(){
        return title;
    }

    public String getBody(){
        return body;
    }

    public String getTag(){
        return tag;
    }

    public String getImg_link(){
        return img_link;
    }

    public String getVisitor(){
        return visitor;
    }
}
