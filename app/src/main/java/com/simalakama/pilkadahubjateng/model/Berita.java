package com.simalakama.pilkadahubjateng.model;

/**
 * Created by Putraa on 4/13/2018.
 */

public class Berita {
    private String textTanggal, textTitleBerita, textDetailBerita, imageBerita;

    public Berita(String imageBerita, String textTanggal, String textTitleBerita, String textDetailBerita){
        this.imageBerita = imageBerita;
        this.textTanggal = textTanggal;
        this.textTitleBerita = textTitleBerita;
        this.textDetailBerita = textDetailBerita;
    }

    public String getImageBerita() {
        return imageBerita;
    }

    public void setImageBerita(String imageBerita) {
        this.imageBerita = imageBerita;
    }

    public String getTextTanggal() {
        return textTanggal;
    }

    public void setTextTanggal(String textTanggal) {
        this.textTanggal = textTanggal;
    }

    public String getTextTitleBerita() {
        return textTitleBerita;
    }

    public void setTextTitleBerita(String textTitleBerita) {
        this.textTitleBerita = textTitleBerita;
    }

    public String getTextDetailBerita() {
        return textDetailBerita;
    }

    public void setTextDetailBerita(String textDetailBerita) {
        this.textDetailBerita = textDetailBerita;
    }
}
