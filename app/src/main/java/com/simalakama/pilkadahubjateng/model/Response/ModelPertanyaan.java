package com.simalakama.pilkadahubjateng.model.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Putraa on 4/19/2018.
 */

public class ModelPertanyaan {
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("nama")
    @Expose
    private String nama;
    @SerializedName("tanyaKe")
    @Expose
    private String tanyaKe;
    @SerializedName("pertanyaan")
    @Expose
    private String pertanyaan;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("tanggal")
    @Expose
    private String tanggal;
    @SerializedName("status")
    @Expose
    private String status;

    public ModelPertanyaan(
            String id, String nama, String tanyaKe, String pertanyaan, String email, String tanggal, String status
    ){
        this.id = id;
        this.nama = nama;
        this.tanyaKe = tanyaKe;
        this.pertanyaan = pertanyaan;
        this.email = email;
        this.tanggal = tanggal;
        this.status = status;
    }
    public ModelPertanyaan(
            String nama, String tanyaKe, String pertanyaan, String email, String tanggal, String status
    ){
        this.nama = nama;
        this.tanyaKe = tanyaKe;
        this.pertanyaan = pertanyaan;
        this.email = email;
        this.tanggal = tanggal;
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public String getId() {
        return id;
    }

    public String getNama() {
        return nama;
    }

    public String getEmail() {
        return email;
    }

    public String getTanyaKe() {
        return tanyaKe;
    }

    public String getPertanyaan() {
        return pertanyaan;
    }

    public String getTanggal() {
        return tanggal;
    }
}
