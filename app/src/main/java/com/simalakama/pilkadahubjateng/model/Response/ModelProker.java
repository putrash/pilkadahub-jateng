package com.simalakama.pilkadahubjateng.model.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Putraa on 4/21/2018.
 */

public class ModelProker {

    @SerializedName("visi")
    @Expose
    private String visi;
    @SerializedName("misi")
    @Expose
    private String misi;
    @SerializedName("program")
    @Expose
    private String program;
    @SerializedName("prestasi")
    @Expose
    private String prestasi;
    @SerializedName("janji")
    @Expose
    private String janji;

    public ModelProker(String visi, String misi, String program, String prestasi, String janji){
        this.visi = visi;
        this.misi = misi;
        this.program = program;
        this.prestasi = prestasi;
        this.janji = janji;
    }

    public String getVisi() {
        return visi;
    }

    public String getMisi() {
        return misi;
    }

    public String getProgram() {
        return program;
    }

    public String getPrestasi() {
        return prestasi;
    }

    public String getJanji() {
        return janji;
    }
}
