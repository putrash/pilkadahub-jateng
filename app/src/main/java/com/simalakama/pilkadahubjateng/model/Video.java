package com.simalakama.pilkadahubjateng.model;

/**
 * Created by Putraa on 5/4/2018.
 */

public class Video {
    private String textJudul, textDuration, imageBerita, videoBerita;

    public Video(String textJudul, String textDuration, String imageBerita, String videoBerita){
        this.textJudul = textJudul;
        this.textDuration = textDuration;
        this.imageBerita = imageBerita;
        this.videoBerita = videoBerita;
    }

    public String getTextJudul() {
        return textJudul;
    }

    public void setTextJudul(String textJudul) {
        this.textJudul = textJudul;
    }

    public String getTextDuration() {
        return textDuration;
    }

    public void setTextDuration(String textDuration) {
        this.textDuration = textDuration;
    }

    public String getImageBerita() {
        return imageBerita;
    }

    public void setImageBerita(String imageBerita) {
        this.imageBerita = imageBerita;
    }

    public String getVideoBerita() {
        return videoBerita;
    }

    public void setVideoBerita(String videoBerita) {
        this.videoBerita = videoBerita;
    }
}
