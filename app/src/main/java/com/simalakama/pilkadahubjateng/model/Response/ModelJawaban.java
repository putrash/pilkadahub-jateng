package com.simalakama.pilkadahubjateng.model.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Putraa on 4/19/2018.
 */

public class ModelJawaban{
    @SerializedName("idPertanyaan")
    @Expose
    private String idPertanyaan;
    @SerializedName("jawaban")
    @Expose
    private String jawaban;
    @SerializedName("img")
    @Expose
    private String img;
    @SerializedName("jawaboleh")
    @Expose
    private String jawabOleh;

    public ModelJawaban(String idPertanyaan, String jawaban, String img, String jawabOleh){
        this.idPertanyaan = idPertanyaan;
        this.jawaban = jawaban;
        this.img = img;
        this.jawabOleh = jawabOleh;
    }

    public String getIdPertanyaan() {
        return idPertanyaan;
    }

    public String getJawaban() {
        return jawaban;
    }

    public String getImg() {
        return img;
    }

    public String getJawabOleh() {
        return jawabOleh;
    }
}
