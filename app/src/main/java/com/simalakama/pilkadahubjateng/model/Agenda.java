package com.simalakama.pilkadahubjateng.model;

/**
 * Created by Putraa on 4/19/2018.
 */

public class Agenda {
    private String textTanggalAwal, textTanggalAkhir, textAgenda;

    public Agenda(String textTanggalAwal, String textTanggalAkhir, String textAgenda){
        this.textTanggalAwal = textTanggalAwal;
        this.textTanggalAkhir = textTanggalAkhir;
        this.textAgenda = textAgenda;
    }

    public String getTextTanggalAwal() {
        return textTanggalAwal;
    }

    public void setTextTanggalAwal(String textTanggalAwal) {
        this.textTanggalAwal = textTanggalAwal;
    }

    public String getTextTanggalAkhir() {
        return textTanggalAkhir;
    }

    public void setTextTanggalAkhir(String textTanggalAkhir) {
        this.textTanggalAkhir = textTanggalAkhir;
    }

    public String getTextAgenda() {
        return textAgenda;
    }

    public void setTextAgenda(String textAgenda) {
        this.textAgenda = textAgenda;
    }
}
