package com.simalakama.pilkadahubjateng.model.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Putraa on 4/21/2018.
 */

public class ModelPaslon {

    @SerializedName("nama_kepala_daerah")
    @Expose
    private String namaKepalaDaerah;
    @SerializedName("gender_kepala_daerah")
    @Expose
    private String genderKepalaDaerah;
    @SerializedName("tempat_lahir_kepala_daerah")
    @Expose
    private String tempatLahirKepalaDaerah;
    @SerializedName("tanggal_lahir_kepala_daerah")
    @Expose
    private String tanggalLahirKepalaDaerah;
    @SerializedName("pekerjaan_kepala_daerah")
    @Expose
    private String pekerjaanKepalaDaerah;
    @SerializedName("nama_wakil_kepala_daerah")
    @Expose
    private String namaWakilKepalaDaerah;
    @SerializedName("gender_wakil_kepala_daerah")
    @Expose
    private String genderWakilKepalaDaerah;
    @SerializedName("tempat_lahir_wakil")
    @Expose
    private String tempatLahirWakilKepalaDaerah;
    @SerializedName("tanggal_lahir_wakil")
    @Expose
    private String tanggalLahirWakilKepalaDaerah;
    @SerializedName("pekerjaan_wakil_kepala_daerah")
    @Expose
    private String pekerjaanWakilKepalaDaerah;
    @SerializedName("jumlah_dukungan")
    @Expose
    private String jumlahDukungan;
    @SerializedName("partai_pendukung")
    @Expose
    private String partaiPendukung;
    @SerializedName("status_pertahana")
    @Expose
    private String statusPertahana;
    @SerializedName("dana_kampanye")
    @Expose
    private String danaKampanye;

    public ModelPaslon(String namaKepalaDaerah, String genderKepalaDaerah, String tempatLahirKepalaDaerah, String tanggalLahirKepalaDaerah, String pekerjaanKepalaDaerah,
                       String namaWakilKepalaDaerah, String genderWakilKepalaDaerah, String tempatLahirWakilKepalaDaerah, String tanggalLahirWakilKepalaDaerah, String pekerjaanWakilKepalaDaerah,
                       String jumlahDukungan, String partaiPendukung, String statusPertahana, String danaKampanye){
        this.namaKepalaDaerah = namaKepalaDaerah;
        this.genderKepalaDaerah = genderKepalaDaerah;
        this.tempatLahirKepalaDaerah = tempatLahirKepalaDaerah;
        this.tanggalLahirKepalaDaerah = tanggalLahirKepalaDaerah;
        this.pekerjaanKepalaDaerah = pekerjaanKepalaDaerah;
        this.namaWakilKepalaDaerah = namaWakilKepalaDaerah;
        this.genderWakilKepalaDaerah = genderWakilKepalaDaerah;
        this.tempatLahirWakilKepalaDaerah = tempatLahirWakilKepalaDaerah;
        this.tanggalLahirWakilKepalaDaerah = tanggalLahirWakilKepalaDaerah;
        this.pekerjaanWakilKepalaDaerah = pekerjaanWakilKepalaDaerah;
        this.jumlahDukungan = jumlahDukungan;
        this.partaiPendukung = partaiPendukung;
        this.statusPertahana = statusPertahana;
        this.danaKampanye = danaKampanye;
    }

    public String getNamaKepalaDaerah() {
        return namaKepalaDaerah;
    }

    public String getGenderKepalaDaerah() {
        return genderKepalaDaerah;
    }

    public String getTempatLahirKepalaDaerah() {
        return tempatLahirKepalaDaerah;
    }

    public String getTanggalLahirKepalaDaerah() {
        return tanggalLahirKepalaDaerah;
    }

    public String getPekerjaanKepalaDaerah() {
        return pekerjaanKepalaDaerah;
    }

    public String getNamaWakilKepalaDaerah() {
        return namaWakilKepalaDaerah;
    }

    public String getGenderWakilKepalaDaerah() {
        return genderWakilKepalaDaerah;
    }

    public String getTempatLahirWakilKepalaDaerah() {
        return tempatLahirWakilKepalaDaerah;
    }

    public String getTanggalLahirWakilKepalaDaerah() {
        return tanggalLahirWakilKepalaDaerah;
    }

    public String getPekerjaanWakilKepalaDaerah() {
        return pekerjaanWakilKepalaDaerah;
    }

    public String getJumlahDukungan() {
        return jumlahDukungan;
    }

    public String getPartaiPendukung() {
        return partaiPendukung;
    }

    public String getDanaKampanye() {
        return danaKampanye;
    }

    public String getStatusPertahana() {
        return statusPertahana;
    }
}
