package com.simalakama.pilkadahubjateng.model.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Putraa on 4/19/2018.
 */

public class ModelAgenda {
    @SerializedName("tgl_akhir")
    @Expose
    private String tgl_akhir;
    @SerializedName("tgl_awal")
    @Expose
    private String tgl_awal;
    @SerializedName("detail")
    @Expose
    private String detail;
    @SerializedName("tahapan")
    @Expose
    private String tahapan;

    public ModelAgenda(String tgl_akhir, String tgl_awal, String detail, String tahapan){
        this.tgl_akhir = tgl_akhir;
        this.tgl_awal = tgl_awal;
        this.detail = detail;
        this.tahapan = tahapan;
    }

    public String getTgl_akhir(){
        return tgl_akhir;
    }

    public String getTgl_awal(){
        return tgl_awal;
    }

    public String getDetail(){
        return detail;
    }

    public String getTahapan(){
        return tahapan;
    }
}
