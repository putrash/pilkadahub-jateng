package com.simalakama.pilkadahubjateng.model.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Putraa on 5/4/2018.
 */

public class ModelVideo {
    @SerializedName("judul")
    @Expose
    private String judul;
    @SerializedName("thumbnail")
    @Expose
    private String thumbnail;
    @SerializedName("video")
    @Expose
    private String video;
    @SerializedName("durasi")
    @Expose
    private String durasi;

    public ModelVideo( String judul, String thumbnail, String video, String durasi){
        this.judul = judul;
        this.thumbnail = thumbnail;
        this.video = video;
        this.durasi = durasi;
    }

    public String getJudul() {
        return judul;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public String getVideo() {
        return video;
    }

    public String getDurasi() {
        return durasi;
    }
}