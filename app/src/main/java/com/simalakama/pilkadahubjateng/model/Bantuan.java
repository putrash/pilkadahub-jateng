package com.simalakama.pilkadahubjateng.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Putraa on 4/14/2018.
 */

public class Bantuan {
    public static HashMap<String,List<String>> getData() {
        HashMap<String,List<String>> bantuan = new HashMap<String,List<String>>();

        List<String> bantuan_pertama = new ArrayList<String>();
        bantuan_pertama.add("Pilkada Hub adalah aplikasi yang memudahkan masyarakat dalam mencari data pemilih, data pdpp, melihat berita, serta melihat rekapan quick count secara realtime.");

        List<String> bantuan_kedua = new ArrayList<String>();
        bantuan_kedua.add("Anda tidak perlu ribet untuk login aplikasi ini, karena aplikasi ini didesain untuk semua orang baik kalangan awam maupun melek teknologi, sehingga tidak membingungkan bagi kalangan awam.");

        List<String> bantuan_ketiga = new ArrayList<String>();
        bantuan_ketiga.add("Anda dapat mengecek di menu DPS, dengan cara mengklik tab dps lalu masukan nama, dan nik anda.");

        List<String> bantuan_keempat = new ArrayList<String>();
        bantuan_keempat.add("Pilkada Hub memiliki menu ppdp yang berisi data petugas tps, untuk mencarinya anda memasukan kabupaten, kecamatan, dan kelurahan yang anda inginkan");

        List<String> bantuan_kelima = new ArrayList<String>();
        bantuan_kelima.add("Data-data mengenai anda memilih di tps berapa dapat di lihat pada menu DPS, salah satu isi dari menu DPS adalah data tentang anda memilih di tps berapa.");

        List<String> bantuan_keenam = new ArrayList<String>();
        bantuan_keenam.add("Pilkada Hub memiliki menu pengaduan, pada menu ini anda dapat mengirim saran atau kritik atau pelanggaran pada proses pemilihan.");

        List<String> bantuan_ketujuh = new ArrayList<String>();
        bantuan_ketujuh.add("Pada menu home aplikasi pilkada hub terdapat menu pasangan calon, ketika anda membukanya maka anda akan disajikan data-data mengani riwayat hidup, partai pendukung, dan visi misi pasangan calon tersebut.");


        bantuan.put("Apa itu pilkada Hub ?",bantuan_pertama);
        bantuan.put("Apakah saya perlu login untuk menggunakan aplikasi ini?",bantuan_kedua);
        bantuan.put("Bagaimana Saya mengecek apakah saya sudah bisa memilih?",bantuan_ketiga);
        bantuan.put("Bagaimana saya mengecek petugas tps?",bantuan_keempat);
        bantuan.put("Bagaimana saya mengecek saya di tps berapa?",bantuan_kelima);
        bantuan.put("Bila saya ingin mengajukan saran atau pengaduan bagaimana caraya?",bantuan_keenam);
        bantuan.put("Bagaimana saya mengetahui data-data pasangan calon?",bantuan_ketujuh);

        return  bantuan;
    }
}
