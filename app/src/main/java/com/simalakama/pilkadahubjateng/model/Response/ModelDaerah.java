package com.simalakama.pilkadahubjateng.model.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Putraa on 4/20/2018.
 */

public class ModelDaerah {

    @SerializedName("kabupaten")
    @Expose
    private List<String> kabupaten;
    @SerializedName("kecamatan")
    @Expose
    private List<String> kecamatan;
    @SerializedName("desa")
    @Expose
    private List<String> desa;

    public ModelDaerah(List<String> kabupaten, List<String> kecamatan, List<String> desa){
        this.kabupaten = kabupaten;
        this.kecamatan = kecamatan;
        this.desa = desa;
    }

    public List<String> getKabupaten() {
        return kabupaten;
    }

    public void setKabupaten(List<String> kabupaten) {
        this.kabupaten = kabupaten;
    }

    public List<String> getKecamatan() {
        return kecamatan;
    }

    public void setKecamatan(List<String> kecamatan) {
        this.kecamatan = kecamatan;
    }

    public List<String> getDesa() {
        return desa;
    }

    public void setDesa(List<String> desa) {
        this.desa = desa;
    }
}
