package com.simalakama.pilkadahubjateng.model.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Putraa on 4/19/2018.
 */

public class ModelLaporan {
    @SerializedName("nama")
    @Expose
    private String nama;
    @SerializedName("alamat")
    @Expose
    private String alamat;
    @SerializedName("no_hp")
    @Expose
    private String no_hp;
    @SerializedName("tipe")
    @Expose
    private String tipe;
    @SerializedName("isi")
    @Expose
    private String isi;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("photo")
    @Expose
    private String[] photo;

    public ModelLaporan(String nama, String alamat, String no_hp, String tipe, String isi, String[] photo, String email){
        this.nama = nama;
        this.alamat = alamat;
        this.no_hp = no_hp;
        this.tipe = tipe;
        this.isi= isi;
        this.photo = photo;
        this.email = email;
    }

    public String getNama(){
        if (nama == null){nama="kosong";}
        return nama;
    }

    public String getIsi(){
        if (isi == null){isi="kosong";}
        return isi;
    }
}
