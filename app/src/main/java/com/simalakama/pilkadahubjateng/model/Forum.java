package com.simalakama.pilkadahubjateng.model;

public class Forum {
    private String idPertanyaan, pertanyaan, tanyaKe, nama, tanggal, status, email, jawaban, img;

    public Forum(String idPertanyaan, String pertanyaan, String tanyaKe, String nama, String tanggal){
        this.idPertanyaan = idPertanyaan;
        this.pertanyaan = pertanyaan;
        this.tanyaKe = tanyaKe;
        this.nama = nama;
        this.tanggal = tanggal;
    }

    public String getNama() {
        return nama;
    }

    public String getTanggal() {
        return tanggal;
    }

    public String getPertanyaan() {
        return pertanyaan;
    }

    public String getIdPertanyaan() {
        return idPertanyaan;
    }

    public String getTanyaKe() {
        return tanyaKe;
    }
}
