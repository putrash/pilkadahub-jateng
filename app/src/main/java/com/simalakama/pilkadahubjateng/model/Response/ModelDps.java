package com.simalakama.pilkadahubjateng.model.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Putraa on 4/19/2018.
 */

public class ModelDps {
    @SerializedName("nama")
    @Expose
    private String nama;
    @SerializedName("nik")
    @Expose
    private String nik;
    @SerializedName("tempatLahir")
    @Expose
    private String tempat_lahir;
    @SerializedName("jenisKelamin")
    @Expose
    private String jenis_kelamin;
    @SerializedName("desa")
    @Expose
    private String desa;
    @SerializedName("tps")
    @Expose
    private String tps;

    public ModelDps(String nama, String nik, String tempat_lahir, String jenis_kelamin, String desa, String tps){
        this.nama = nama;
        this.nik = nik;
        this.tempat_lahir = tempat_lahir;
        this.jenis_kelamin = jenis_kelamin;
        this.desa = desa;
        this.tps = tps;
    }

    public String getNama(){
        return nama;
    }

    public String getNik(){
        return nik;
    }

    public String getTempat_lahir(){
        return tempat_lahir;
    }

    public String getJenis_kelamin(){
        return jenis_kelamin;
    }

    public String getDesa(){
        return desa;
    }

    public String getTps(){
        return tps;
    }
}
