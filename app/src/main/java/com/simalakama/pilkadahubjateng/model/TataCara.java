package com.simalakama.pilkadahubjateng.model;

/**
 * Created by Putraa on 4/15/2018.
 */

public class TataCara {
    private int imageTataCara;
    private int textUrutan;

    public TataCara(int imageTataCara, int textUrutan ){
        this.imageTataCara = imageTataCara;
        this.textUrutan = textUrutan;
    }

    public int getImageTataCara() {
        return imageTataCara;
    }

    public void setImageTataCara(int imageTataCara) {
        this.imageTataCara = imageTataCara;
    }

    public int getTextUrutan() {
        return textUrutan;
    }

    public void setTextUrutan(int textUrutan) {
        this.textUrutan = textUrutan;
    }
}
