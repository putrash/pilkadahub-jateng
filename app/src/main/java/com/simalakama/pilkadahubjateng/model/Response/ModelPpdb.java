package com.simalakama.pilkadahubjateng.model.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Putraa on 4/19/2018.
 */

public class ModelPpdb {
    @SerializedName("kabupaten")
    @Expose
    private String kabupaten;
    @SerializedName("kecamatan")
    @Expose
    private String kecamatan;
    @SerializedName("desa")
    @Expose
    private String desa;
    @SerializedName("tps")
    @Expose
    private String tps;
    @SerializedName("nama")
    @Expose
    private String nama;

    public ModelPpdb(String kabupaten, String kecamatan, String desa, String tps, String nama){
        this.kabupaten = kabupaten;
        this.kecamatan = kecamatan;
        this.desa = desa;
        this.tps = tps;
        this.nama = nama;
    }

    public String getKabupaten(){
        return kabupaten;
    }

    public String getKecamatan(){
        return kecamatan;
    }

    public String getDesa(){
        return desa;
    }

    public String getTps(){
        return tps;
    }

    public String getNama(){
        return nama;
    }
}
