package com.simalakama.pilkadahubjateng.model;

/**
 * Created by Putraa on 4/20/2018.
 */

public class HasilPpdb {

    private String textNo, textNama, textTPS;

    public HasilPpdb(String textNo, String textNama, String textTPS){
        this.textNo = textNo;
        this.textNama = textNama;
        this.textTPS = textTPS;
    }

    public String getTextNo() {
        return textNo;
    }

    public void setTextNo(String textNo) {
        this.textNo = textNo;
    }

    public String getTextNama() {
        return textNama;
    }

    public void setTextNama(String textNama) {
        this.textNama = textNama;
    }

    public String getTextTPS() {
        return textTPS;
    }

    public void setTextTPS(String textTPS) {
        this.textTPS = textTPS;
    }
}
