package com.simalakama.pilkadahubjateng;

import android.Manifest;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.simalakama.pilkadahubjateng.data.ApiService;
import com.simalakama.pilkadahubjateng.model.Response.ModelDps;
import com.simalakama.pilkadahubjateng.utils.ScreenShootUtils;

import java.io.File;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HasilDpsActivity extends AppCompatActivity {

    public static final String NAMA = "nama";
    public static final String NIK= "nik";
    private Toolbar toolbar;
    private String nama,nik;
    private TextView textNIK, textNama, textTPS, textKelamin, textTempatLahir;
    private View rootView;
    private LinearLayout viewHasil, viewEmpty;
    private RelativeLayout viewProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hasil_dps);

        rootView = getWindow().getDecorView().findViewById(android.R.id.content);

        //Get Intent Extra
        nama = getIntent().getExtras().getString(NAMA);
        nik = getIntent().getStringExtra(NIK);
        //Log.d("onCreate", "onCreate: " + nama + " " + nik);

        initView();
        initToolbar();

        viewProgress.setVisibility(View.VISIBLE);
        checkData();
        getHasilDps();
    }

    void initView(){
        toolbar = findViewById(R.id.toolbar);
        //
        viewHasil = findViewById(R.id.view_hasil);
        viewEmpty = findViewById(R.id.view_empty);
        viewProgress = findViewById(R.id.view_progress);
        //
        textNIK = findViewById(R.id.text_hasilnik);
        textNama = findViewById(R.id.text_hasilnama);
        textTPS = findViewById(R.id.text_tps);
        textKelamin = findViewById(R.id.text_kelamin);
        textTempatLahir = findViewById(R.id.text_tempatlahir);
    }

    void initToolbar(){
        // Set a Toolbar to replace the ActionBar and add Padding.
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Hasil Pencarian Data DPS");
        toolbar.setPadding(0,getToolBarHeight(),0,0);
        toolbar.setNavigationIcon(R.drawable.icon_close);
    }

    void checkData(){
        //Check if nik is correct
        if (nik.length() < 13){
            textNIK.setText("");
            textNama.setText("");
            showMessageBoxFinish("Anda memasukan nik yang salah");
        } else {
            textNIK.setText(nik);
            textNama.setText(nama);
            nik = nik.substring(0,12) + "****";
        }
    }

    void getHasilDps(){
        Call<List<ModelDps>> dpsCall = ApiService.service.getDps(nik,nama);
        dpsCall.enqueue(new Callback<List<ModelDps>>() {
            @Override
            public void onResponse(Call<List<ModelDps>> call, Response<List<ModelDps>> response) {
                if (response.isSuccessful()){
                    Log.d("Successfull", String.valueOf(response.body()));
                    List<ModelDps> modelDpsList = response.body();

                    if (modelDpsList.size() != 0){
                        //Change view if response have a data
                        viewHasil.setVisibility(View.VISIBLE);
                        viewEmpty.setVisibility(View.GONE);
                        //Set Text
                        textTPS.setText(modelDpsList.get(0).getTps());
                        textKelamin.setText(modelDpsList.get(0).getJenis_kelamin());
                        textTempatLahir.setText(modelDpsList.get(0).getTempat_lahir());
                    } else {
                        viewHasil.setVisibility(View.GONE);
                        viewEmpty.setVisibility(View.VISIBLE);
                    }

                    viewProgress.setVisibility(View.GONE);

                } else {
                    Log.d("onResponse but Failure", String.valueOf(response.code()));
                }
            }

            @Override
            public void onFailure(Call<List<ModelDps>> call, Throwable t) {
                Log.d("onFailure", "Something goes wrong" + t.toString());
            }
        });
    }

    private void showMessageBoxFinish(String message) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this,R.style.customAlertDialog);
        alertDialogBuilder.setTitle("PilkadaHub");
        alertDialogBuilder.setMessage(message);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton("OK", (dialogInterface, i) -> {
            dialogInterface.dismiss();
            finish();
        });
        alertDialogBuilder.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_screenshoot, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                finish();
                break;
            case R.id.menu_screenshoot:

                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {

                    ActivityCompat.requestPermissions(HasilDpsActivity.this,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            1);

                } else {
                    Log.i("Permission", "STORAGE permission has already been granted.\")");

                    Bitmap bitmap = ScreenShootUtils.getScreenShot(rootView);
                    if (bitmap != null) {

                        File file = ScreenShootUtils.store(bitmap, "Pilkadahub_" + System.currentTimeMillis() + ".jpg");
                        Toast.makeText(this, "You have successfully screenshoot", Toast.LENGTH_SHORT).show();
                        //Log.d("Screenshoot", "createdAt: " + file);

                    } else {
                        Toast.makeText(this, "Opss Something wrong", Toast.LENGTH_SHORT).show();
                    }
                }

                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(HasilDpsActivity.this, "Permission denied to read your External storage", Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }

    private int getToolBarHeight() {
        int height;

        Resources myResources = getResources();
        int idStatusBarHeight = myResources.getIdentifier(
                "status_bar_height", "dimen", "android");
        if (idStatusBarHeight > 0) {
            height = getResources().getDimensionPixelSize(idStatusBarHeight);
            //Toast.makeText(this, "Status Bar Height = " + height, Toast.LENGTH_LONG).show();
        }else{
            height = 0;
            //Toast.makeText(getActivity(), "Resources NOT found", Toast.LENGTH_LONG).show();
        }

        return height;
    }
}
