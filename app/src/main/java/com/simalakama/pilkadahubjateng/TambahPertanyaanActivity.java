package com.simalakama.pilkadahubjateng;

import android.content.DialogInterface;
import android.content.res.Resources;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.simalakama.pilkadahubjateng.custom.CustomDialog;
import com.simalakama.pilkadahubjateng.data.ApiService;
import com.simalakama.pilkadahubjateng.model.Response.ModelLaporan;
import com.simalakama.pilkadahubjateng.model.Response.ModelPertanyaan;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TambahPertanyaanActivity extends AppCompatActivity {
    private Spinner spinnerTujuan;

    private Button btnKirim;
    private EditText textNama, textIsi, textEmail;
    private RelativeLayout progressbar;
    private String tujuan = "";
    private int ID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_pertanyaan);

        ID = getIntent().getIntExtra("PASLON",0);


        // Set a Toolbar to replace the ActionBar and add Padding.
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setPadding(0, getToolBarHeight(), 0, 0);

        // Init UI
        btnKirim = findViewById(R.id.btn_kirim);
        textNama = findViewById(R.id.text_nama);
        textIsi = findViewById(R.id.text_isi);
        textEmail = findViewById(R.id.text_email);
        spinnerTujuan = findViewById(R.id.spinner_tujuan);
        progressbar = findViewById(R.id.progressbar);

        //Initiate Button Kirim
        btnKirim.setOnClickListener(v -> {
            if (textNama.getText().toString().matches("") || textEmail.getText().toString().matches("") ||
                    textIsi.getText().toString().matches("") || tujuan.matches("Semua / Paslon 1 / Paslon 2")) {
                showMessageBox("Mohon data pertanyaan dilengkapi");
            } else {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                alertDialogBuilder.setTitle("PilkadaHub");
                alertDialogBuilder
                        .setMessage("Apakah anda yakin?")
                        .setCancelable(false)
                        .setPositiveButton("Ya", (dialog, id) -> {
                            kirimData();
                        })
                        .setNegativeButton("Tidak", (dialog, id) -> dialog.cancel());
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        });
        dataPertanyaan();
    }

    private void kirimData(){
        progressbar.setVisibility(View.VISIBLE);
        String nama = textNama.getText().toString();
        String pertanyaan = textIsi.getText().toString();
        String email = textEmail.getText().toString();
        String tanggal = new SimpleDateFormat("dd MMMM yyyy").format(Calendar.getInstance().getTime());
        ModelPertanyaan modelPertanyaan = new ModelPertanyaan(nama, tujuan, pertanyaan, email, tanggal, "1");
        Call<ModelLaporan> call = ApiService.service.tanya(modelPertanyaan);
        call.enqueue(new Callback<ModelLaporan>() {
            @Override
            public void onResponse(Call<ModelLaporan> call, Response<ModelLaporan> response) {
                progressbar.setVisibility(View.GONE);
                //Toast.makeText(TambahPertanyaanActivity.this, "Berhasil", Toast.LENGTH_SHORT).show();
                CustomDialog customDialog = new CustomDialog(TambahPertanyaanActivity.this,"Berhasil bertanya");
                customDialog.show();
                customDialog.setOnDismissListener(dialog -> finish());
            }

            @Override
            public void onFailure(Call<ModelLaporan> call, Throwable t) {
                progressbar.setVisibility(View.GONE);
                Toast.makeText(TambahPertanyaanActivity.this, "Jaringan Error!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void showMessageBox(String message) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this, R.style.customAlertDialog);
        alertDialogBuilder.setTitle("PilkadaHub");
        alertDialogBuilder.setMessage(message);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton("OK", (dialogInterface, i) -> {
            dialogInterface.dismiss();
        });
        alertDialogBuilder.show();
    }

    void dataPertanyaan() {
        String[] tuju = {"Semua / Paslon 1 / Paslon 2", "Semua", "Paslon 1 (Ganjar - Yasin)", "Paslon 2 (Sudirman - Ida)"};
        ArrayAdapter<String> spinnerTujuanAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, tuju) {
            @Override
            public boolean isEnabled(int position) {
                return position != 0;
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView textView = (TextView) view;
                if (position == 0) {
                    textView.setTextColor(Color.parseColor("#b4c3cf"));
                } else {
                    textView.setTextColor(Color.parseColor("#2b2f32"));
                }

                return view;
            }
        };
        spinnerTujuanAdapter.setDropDownViewResource(R.layout.spinner_item);
        spinnerTujuan.setAdapter(spinnerTujuanAdapter);
        spinnerTujuan.setSelection(ID);

        spinnerTujuan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                tujuan = spinnerTujuan.getItemAtPosition(position).toString();


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private int getToolBarHeight() {
        int height;

        Resources myResources = getResources();
        int idStatusBarHeight = myResources.getIdentifier(
                "status_bar_height", "dimen", "android");
        if (idStatusBarHeight > 0) {
            height = getResources().getDimensionPixelSize(idStatusBarHeight);
        } else {
            height = 0;
        }

        return height;
    }
}
