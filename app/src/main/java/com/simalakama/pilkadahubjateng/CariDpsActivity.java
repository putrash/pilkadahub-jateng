package com.simalakama.pilkadahubjateng;

import android.content.Intent;
import android.content.res.Resources;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class CariDpsActivity extends AppCompatActivity {

    private Button btnCari;
    private EditText textNIK, textNama;
    private String nik;
    private String nama;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cari_dps);

        // Set a Toolbar to replace the ActionBar and add Padding.
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Daftar Pemilih Sementara");
        toolbar.setPadding(0,getToolBarHeight(),0,0);

        // Init UI
        textNIK = findViewById(R.id.text_nik);
        textNama = findViewById(R.id.text_namalengkap);

        // Initate Button Cari
        btnCari = findViewById(R.id.btn_cari);
        btnCari.setOnClickListener(view -> {
            nik = textNIK.getText().toString().toUpperCase();
            nama = textNama.getText().toString().toUpperCase();
            if (!nik.isEmpty() && !nama.isEmpty()){
                Intent intent = new Intent(CariDpsActivity.this, HasilDpsActivity.class);
                intent.putExtra(HasilDpsActivity.NIK,nik);
                intent.putExtra(HasilDpsActivity.NAMA,nama);
                startActivity(intent);
            } else {
                showMessageBox("Nama atau NIK anda ada yang belum diisi ");
            }
        });

    }

    private void showMessageBox(String message) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this,R.style.customAlertDialog);
        alertDialogBuilder.setTitle("PilkadaHub");
        alertDialogBuilder.setMessage(message);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton("OK", (dialogInterface, i) -> dialogInterface.dismiss());
        alertDialogBuilder.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private int getToolBarHeight() {
        int height;

        Resources myResources = getResources();
        int idStatusBarHeight = myResources.getIdentifier(
                "status_bar_height", "dimen", "android");
        if (idStatusBarHeight > 0) {
            height = getResources().getDimensionPixelSize(idStatusBarHeight);
            //Toast.makeText(this, "Status Bar Height = " + height, Toast.LENGTH_LONG).show();
        }else{
            height = 0;
            //Toast.makeText(getActivity(), "Resources NOT found", Toast.LENGTH_LONG).show();
        }

        return height;
    }
}
