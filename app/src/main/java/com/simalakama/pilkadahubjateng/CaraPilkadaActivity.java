package com.simalakama.pilkadahubjateng;

import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;

import com.github.rubensousa.gravitysnaphelper.GravitySnapHelper;
import com.simalakama.pilkadahubjateng.adapter.RecycleCaraPilkadaAdapter;
import com.simalakama.pilkadahubjateng.model.TataCara;

import java.util.ArrayList;
import java.util.List;

public class CaraPilkadaActivity extends AppCompatActivity {

    private List<TataCara> tataCaraList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cara_pilkada);

        //Get Data
        dataTataCara();

        // Set a Toolbar to replace the ActionBar and add Padding.
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setPadding(0,getToolBarHeight(),0,0);

        //Initiate RecycleView Video Berita
        RecyclerView recyclerView = findViewById(R.id.recycle_carapilkada);

        recyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);

        SnapHelper snapHelper = new GravitySnapHelper(Gravity.START);
        snapHelper.attachToRecyclerView(recyclerView);

        RecycleCaraPilkadaAdapter recycleCaraPilkadaAdapter = new RecycleCaraPilkadaAdapter(tataCaraList);
        recyclerView.setAdapter(recycleCaraPilkadaAdapter);
    }

    private void dataTataCara() {
        tataCaraList.add(new TataCara(R.drawable.image_tatacara1,1));
        tataCaraList.add(new TataCara(R.drawable.image_tatacara2,2));
        tataCaraList.add(new TataCara(R.drawable.image_tatacara3,3));
        tataCaraList.add(new TataCara(R.drawable.image_tatacara4,4));
        tataCaraList.add(new TataCara(R.drawable.image_tatacara5,5));
        tataCaraList.add(new TataCara(R.drawable.image_tatacara6,6));
        tataCaraList.add(new TataCara(R.drawable.image_tatacara7,7));

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private int getToolBarHeight() {
        int height;

        Resources myResources = getResources();
        int idStatusBarHeight = myResources.getIdentifier(
                "status_bar_height", "dimen", "android");
        if (idStatusBarHeight > 0) {
            height = getResources().getDimensionPixelSize(idStatusBarHeight);
            //Toast.makeText(getActivity(), "Status Bar Height = " + height, Toast.LENGTH_LONG).show();
        } else {
            height = 0;
            //Toast.makeText(getActivity(), "Resources NOT found", Toast.LENGTH_LONG).show();
        }

        return height;
    }
}
