package com.simalakama.pilkadahubjateng;

import android.content.res.Resources;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.support.v7.widget.Toolbar;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.github.rubensousa.gravitysnaphelper.GravitySnapHelper;
import com.simalakama.pilkadahubjateng.adapter.RecycleBeritaLainnyaAdapter;
import com.simalakama.pilkadahubjateng.custom.CustomDividerItemDecoration;
import com.simalakama.pilkadahubjateng.data.ApiService;
import com.simalakama.pilkadahubjateng.model.Berita;
import com.simalakama.pilkadahubjateng.model.Response.ModelBerita;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BeritaPilkadaActivity extends AppCompatActivity {

    private List<Berita> beritaList = new ArrayList<>();
    private RecyclerView recyclerViewBerita;

    private RecycleBeritaLainnyaAdapter recycleBeritaLainnyaAdapter;
    private String tanggal, isiBerita;
    private SwipeRefreshLayout swipeRefresh;
    private ShimmerFrameLayout shimmerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_berita_pilkada);

        // Init Shimmer
        shimmerView = findViewById(R.id.view_shimer);

        // Set a Toolbar to replace the ActionBar and add Padding.
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setPadding(0, getToolBarHeight(), 0, 0);

        //Initiate RecycleView Berita Lainnya
        recyclerViewBerita = findViewById(R.id.recycle_beritalainnya);
        recyclerViewBerita.setHasFixedSize(true);
        recyclerViewBerita.addItemDecoration(new CustomDividerItemDecoration(this, LinearLayoutManager.VERTICAL, 18));
        recyclerViewBerita.setNestedScrollingEnabled(false);

        RecyclerView.LayoutManager layoutManagerBerita = new LinearLayoutManager(getApplicationContext());
        recyclerViewBerita.setLayoutManager(layoutManagerBerita);

        SnapHelper snapHelperTop = new GravitySnapHelper(Gravity.TOP);
        snapHelperTop.attachToRecyclerView(recyclerViewBerita);

        recycleBeritaLainnyaAdapter = new RecycleBeritaLainnyaAdapter(beritaList);
        recyclerViewBerita.setAdapter(recycleBeritaLainnyaAdapter);

        //Get Data
        getBerita();

        swipeRefresh = findViewById(R.id.swipe_refresh);
        swipeRefresh.setColorSchemeColors(getResources().getColor(R.color.colorPrimaryDark));
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshItem();
            }

            void refreshItem(){
                getBerita();
                onItemLoad();
            }

            void onItemLoad(){
                swipeRefresh.setRefreshing(false);
            }
        });

    }

    void getBerita(){
        Call<List<ModelBerita>> beritaCall = ApiService.service.getBerita();
        beritaCall.enqueue(new Callback<List<ModelBerita>>() {
            @Override
            public void onResponse(Call<List<ModelBerita>> call, Response<List<ModelBerita>> response) {
                if (response.isSuccessful()){
                    //Log.d("Successfull", String.valueOf(response.body()));
                    List<ModelBerita> modelBeritaList = response.body();
                    recycleBeritaLainnyaAdapter.clear();

                    for (int i = 0; i < response.body().size() ; i++) {

                        //Format Data
                        String stringTanggal = modelBeritaList.get(i).getCreated_at();
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
                        try {
                            Date dateTanggal = simpleDateFormat.parse(stringTanggal);
                            simpleDateFormat = new SimpleDateFormat("dd MMMM yyyy");
                            tanggal = simpleDateFormat.format(dateTanggal);
                            Log.d("onFormat: ", tanggal);

                        } catch (ParseException e){
                            e.printStackTrace();
                        }

                        //Format Isi Berita
                        String stringBerita = modelBeritaList.get(i).getBody();
                        String htmlBerita = Html.fromHtml(stringBerita).toString();
                        isiBerita = Html.fromHtml(htmlBerita).toString();

                        beritaList.add(new Berita(modelBeritaList.get(i).getImg_link(), tanggal, modelBeritaList.get(i).getTitle(), isiBerita));

                    }
                    shimmerView.stopShimmerAnimation();
                    shimmerView.setVisibility(View.GONE);
                    recycleBeritaLainnyaAdapter.notifyDataSetChanged();

                } else {
                    Log.d("onResponse but Failure", String.valueOf(response.code()));
                }
            }

            @Override
            public void onFailure(Call<List<ModelBerita>> call, Throwable t) {
                Log.d("onFailure", "Something goes wrong" + t.toString());
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        shimmerView.startShimmerAnimation();
    }

    @Override
    public void onPause() {
        shimmerView.stopShimmerAnimation();
        super.onPause();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private int getToolBarHeight() {
        int height;

        Resources myResources = getResources();
        int idStatusBarHeight = myResources.getIdentifier(
                "status_bar_height", "dimen", "android");
        if (idStatusBarHeight > 0) {
            height = getResources().getDimensionPixelSize(idStatusBarHeight);
            //Toast.makeText(getActivity(), "Status Bar Height = " + height, Toast.LENGTH_LONG).show();
        } else {
            height = 0;
            //Toast.makeText(getActivity(), "Resources NOT found", Toast.LENGTH_LONG).show();
        }

        return height;
    }

}
