package com.simalakama.pilkadahubjateng;

import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class DetailBeritaActivity extends AppCompatActivity {

    public static final String TITLE_BERITA = "title_berita";
    public static final String ISI_BERITA = "isi_berita";
    public static final String TANGGAL_BERITA = "tanggal_berita";
    public static final String COVER_BERITA = "cover_berita";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_berita);

        String title = getIntent().getStringExtra(TITLE_BERITA);
        String isi = getIntent().getStringExtra(ISI_BERITA);
        String tanggal = getIntent().getStringExtra(TANGGAL_BERITA);
        String cover = getIntent().getStringExtra(COVER_BERITA);

        // Set a Toolbar to replace the ActionBar and add Padding.
        final Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //Init UI
        TextView textIsi = findViewById(R.id.text_isiberita);
        textIsi.setText(isi);

        TextView textTanggal = findViewById(R.id.text_tanggalberita);
        textTanggal.setText(tanggal);

        ImageView imageBerita = findViewById(R.id.image_berita);
        Picasso.get()
                .load(cover)
                .placeholder(R.color.colorPrimaryDark)
                .into(imageBerita);

        net.opacapp.multilinecollapsingtoolbar.CollapsingToolbarLayout collapsingToolbarLayout = findViewById(R.id.collapsing_toolbar);
        collapsingToolbarLayout.setStatusBarScrim(getResources().getDrawable(R.drawable.color_gradient));
        collapsingToolbarLayout.setContentScrim(getResources().getDrawable(R.drawable.color_gradient));
        collapsingToolbarLayout.setTitle(title);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private int getToolBarHeight() {
        int height;

        Resources myResources = getResources();
        int idStatusBarHeight = myResources.getIdentifier(
                "status_bar_height", "dimen", "android");
        if (idStatusBarHeight > 0) {
            height = getResources().getDimensionPixelSize(idStatusBarHeight);
            //Toast.makeText(this, "Status Bar Height = " + height, Toast.LENGTH_LONG).show();
        }else{
            height = 0;
            //Toast.makeText(getActivity(), "Resources NOT found", Toast.LENGTH_LONG).show();
        }

        return height;
    }
}
