package com.simalakama.pilkadahubjateng;

import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;


import com.simalakama.pilkadahubjateng.data.ApiService;
import com.simalakama.pilkadahubjateng.model.Response.ModelProker;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VisiMisiActivity extends AppCompatActivity {

    private String id, paslon;
    private TextView textVisi, textMisi, textPaslon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visi_misi);

        id = getIntent().getStringExtra("ID");
        if (id.equals("1")){
            id = "5ad058bca11f7615e82648bf";
            paslon = "Ganjar - Yasin";
        } else if (id.equals("2")){
            id = "5ad059b0a11f7615e82648c0";
            paslon = "Sudirman - Ida";
        }

        // Set a Toolbar to replace the ActionBar and add Padding.
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setPadding(0,getToolBarHeight(),0,0);
        toolbar.setNavigationIcon(R.drawable.icon_close);

        // Init UI
        textVisi = findViewById(R.id.text_visi);
        textMisi = findViewById(R.id.text_misi);
        textPaslon = findViewById(R.id.text_paslon);
        textPaslon.setText(paslon);

        getProkerPaslon();

    }

    void getProkerPaslon(){
        Call<ModelProker> prokerCall = ApiService.service.getProkerPaslon(id);
        prokerCall.enqueue(new Callback<ModelProker>() {
            @Override
            public void onResponse(Call<ModelProker> call, Response<ModelProker> response) {
                if (response.isSuccessful()){
                    Log.d("Successfull", String.valueOf(response.body()));
                    ModelProker modelProker = response.body();

                    textMisi.setText(String.valueOf(modelProker.getMisi()));
                    textVisi.setText(String.valueOf(modelProker.getVisi()));

                } else {
                    Log.d("onResponse but Failure", String.valueOf(response.code()));
                }
            }

            @Override
            public void onFailure(Call<ModelProker> call, Throwable t) {
                Log.d("onFailure", "Something goes wrong" + t.toString());
            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private int getToolBarHeight() {
        int height;

        Resources myResources = getResources();
        int idStatusBarHeight = myResources.getIdentifier(
                "status_bar_height", "dimen", "android");
        if (idStatusBarHeight > 0) {
            height = getResources().getDimensionPixelSize(idStatusBarHeight);
            //Toast.makeText(this, "Status Bar Height = " + height, Toast.LENGTH_LONG).show();
        }else{
            height = 0;
            //Toast.makeText(getActivity(), "Resources NOT found", Toast.LENGTH_LONG).show();
        }

        return height;
    }
}
