package com.simalakama.pilkadahubjateng;

import android.content.Intent;
import android.content.res.Resources;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.simalakama.pilkadahubjateng.adapter.ListForumAdapter;
import com.simalakama.pilkadahubjateng.adapter.TabAdapter;
import com.simalakama.pilkadahubjateng.data.ApiService;
import com.simalakama.pilkadahubjateng.fragment.ForumPaslon1Fragment;
import com.simalakama.pilkadahubjateng.fragment.ForumPaslon2Fragment;
import com.simalakama.pilkadahubjateng.fragment.ForumSemuaFragment;
import com.simalakama.pilkadahubjateng.fragment.JawabanSemua1Fragment;
import com.simalakama.pilkadahubjateng.fragment.JawabanSemua2Fragment;
import com.simalakama.pilkadahubjateng.model.Forum;
import com.simalakama.pilkadahubjateng.model.Response.ModelJawaban;
import com.simalakama.pilkadahubjateng.model.Response.ModelPertanyaan;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class JawabanActivity extends AppCompatActivity {
    private TextView pertanyaan, nama, tanggal, paslon, jawaban;
    private ImageView gambar;
    private LinearLayout container1, container2;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jawaban);

        // Set a Toolbar to replace the ActionBar and add Padding.
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setPadding(0, getToolBarHeight(), 0, 0);

        // Init UI
        pertanyaan = findViewById(R.id.pertanyaan);
        nama = findViewById(R.id.nama);
        tanggal = findViewById(R.id.tanggal);
        paslon = findViewById(R.id.paslon);
        jawaban = findViewById(R.id.jawaban);
        gambar = findViewById(R.id.image);
        container1 = findViewById(R.id.container1);
        container2 = findViewById(R.id.container2);
        viewPager = findViewById(R.id.viewpager_jawaban);

        // Get Extra
        Intent i = getIntent();
        nama.setText(i.getStringExtra("nama"));
        tanggal.setText(i.getStringExtra("tanggal"));
        pertanyaan.setText(i.getStringExtra("pertanyaan"));
        String tanyake = i.getStringExtra("tanyake");
        String id = i.getStringExtra("id");

        container1.setVisibility(View.VISIBLE);
        container2.setVisibility(View.GONE);

        if (tanyake.equals("Paslon 1 (Ganjar - Yasin)")){
            paslon.setText("Jawaban dari Paslon 1");
            getData(id);
        }else if(tanyake.equals("Paslon 2 (Sudirman - Ida)")){
            paslon.setText("Jawaban dari Paslon 2");
            getData(id);
        }else{
            container1.setVisibility(View.GONE);
            container2.setVisibility(View.VISIBLE);
            Bundle bundle = new Bundle();
            bundle.putString("id", id);
            JawabanSemua1Fragment fragment1 = new JawabanSemua1Fragment();
            fragment1.setArguments(bundle);
            JawabanSemua2Fragment fragment2 = new JawabanSemua2Fragment();
            fragment2.setArguments(bundle);
            ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
            viewPagerAdapter.addFragment(fragment1,"Ganjar - Yasin (1)");
            viewPagerAdapter.addFragment(fragment2,"Sudirman - Ida (2)");
            viewPager.setAdapter(viewPagerAdapter);
            TabLayout tabLayout = findViewById(R.id.tab_jawaban);
            tabLayout.setupWithViewPager(viewPager);
        }
    }

    private void getData(String id){
        final Call<List<ModelJawaban>> jawabanCall = ApiService.service.getJawaban();
        jawabanCall.enqueue(new Callback<List<ModelJawaban>>() {
            @Override
            public void onResponse(Call<List<ModelJawaban>> call, Response<List<ModelJawaban>> response) {
                if (response.isSuccessful()) {
                    List<ModelJawaban> modelJawabanList = response.body();
                    for (int i = 0; i < modelJawabanList.size(); i++) {
                        if (modelJawabanList.get(i).getIdPertanyaan().equals(id)){
                            jawaban.setText(modelJawabanList.get(i).getJawaban());
                        }
                    }
                } else {
                    Log.d("onResponse but Failure", String.valueOf(response.code()));
                }
            }

            @Override
            public void onFailure(Call<List<ModelJawaban>> call, Throwable t) {
                Log.d("onFailure", "Something goes wrong" + t.toString());
            }
        });
    }

    private int getToolBarHeight() {
        int height;

        Resources myResources = getResources();
        int idStatusBarHeight = myResources.getIdentifier(
                "status_bar_height", "dimen", "android");
        if (idStatusBarHeight > 0) {
            height = getResources().getDimensionPixelSize(idStatusBarHeight);
        } else {
            height = 0;
        }

        return height;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {finish();}
        return super.onOptionsItemSelected(item);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}