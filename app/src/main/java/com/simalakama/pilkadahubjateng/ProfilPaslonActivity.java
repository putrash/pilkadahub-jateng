package com.simalakama.pilkadahubjateng;

import android.content.res.Resources;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.simalakama.pilkadahubjateng.adapter.TabAdapter;
import com.simalakama.pilkadahubjateng.fragment.ProfilGubernurFragment;
import com.simalakama.pilkadahubjateng.fragment.ProfilWakilGubernurFragment;

public class ProfilPaslonActivity extends AppCompatActivity {

    private String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profil_paslon);

        id = getIntent().getStringExtra("ID");

        // Set a Toolbar to replace the ActionBar and add Padding.
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setPadding(0,getToolBarHeight(),0,0);
        toolbar.setNavigationIcon(R.drawable.icon_close);

        TabAdapter tabAdapter = new TabAdapter(getSupportFragmentManager());
        Bundle bundle = new Bundle();
        bundle.putString("ID", id);

        ProfilGubernurFragment profilGubernur = new ProfilGubernurFragment();
        profilGubernur.setArguments(bundle);
        ProfilWakilGubernurFragment profilWakilGubernur = new ProfilWakilGubernurFragment();
        profilWakilGubernur.setArguments(bundle);

        tabAdapter.addFragment(profilGubernur,"Gubernur");
        tabAdapter.addFragment(profilWakilGubernur,"Wakil Gubernur");

        ViewPager viewPager = findViewById(R.id.viewpager_paslon);
        viewPager.setAdapter(tabAdapter);

        TabLayout tabLayout = findViewById(R.id.tab_paslon);
        tabLayout.setTabTextColors(getResources().getColor(R.color.colorTitleTab),getResources().getColor(android.R.color.white));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.setupWithViewPager(viewPager);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private int getToolBarHeight() {
        int height;

        Resources myResources = getResources();
        int idStatusBarHeight = myResources.getIdentifier(
                "status_bar_height", "dimen", "android");
        if (idStatusBarHeight > 0) {
            height = getResources().getDimensionPixelSize(idStatusBarHeight);
            //Toast.makeText(getActivity(), "Status Bar Height = " + height, Toast.LENGTH_LONG).show();
        }else{
            height = 0;
            //Toast.makeText(getActivity(), "Resources NOT found", Toast.LENGTH_LONG).show();
        }

        return height;
    }
}
