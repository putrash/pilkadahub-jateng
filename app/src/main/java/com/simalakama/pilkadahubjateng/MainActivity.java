package com.simalakama.pilkadahubjateng;

import android.support.v4.app.Fragment;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.widget.Toast;

import com.simalakama.pilkadahubjateng.fragment.AgendaFragment;
import com.simalakama.pilkadahubjateng.fragment.ForumFragment;
import com.simalakama.pilkadahubjateng.fragment.HomeFragment;
import com.simalakama.pilkadahubjateng.fragment.LainnyaFragment;
import com.simalakama.pilkadahubjateng.fragment.PemilihFragment;
import com.simalakama.pilkadahubjateng.fragment.PpdpFragment;

import java.lang.reflect.Field;

public class MainActivity extends AppCompatActivity {
    Fragment selectedFragment = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_navigation);
        BottomNavigationViewHelper.disableShiftMode(bottomNavigationView);

        bottomNavigationView.setOnNavigationItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.beranda:
                    selectedFragment = HomeFragment.newInstance();
                    break;
                case R.id.pemilih:
                    selectedFragment = PemilihFragment.newInstance();
                    break;
                case R.id.ppdp:
                    selectedFragment = PpdpFragment.newInstance();
                    break;
                case R.id.forum:
                    selectedFragment = ForumFragment.newInstance();
                    break;
                case R.id.lainnya:
                    selectedFragment = LainnyaFragment.newInstance();
                    break;
            }
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.frame_layout, selectedFragment)
                    .commit();
            return true;
        });

        if (savedInstanceState == null) {
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.frame_layout, HomeFragment.newInstance())
                        .commit();
        }

    }

    public static class BottomNavigationViewHelper {
        public static void disableShiftMode(BottomNavigationView view) {
            BottomNavigationMenuView menuView = (BottomNavigationMenuView) view.getChildAt(0);
            try {
                Field shiftingMode = menuView.getClass().getDeclaredField("mShiftingMode");
                shiftingMode.setAccessible(true);
                shiftingMode.setBoolean(menuView, false);
                shiftingMode.setAccessible(false);
                for (int i = 0; i < menuView.getChildCount(); i++) {
                    BottomNavigationItemView item = (BottomNavigationItemView) menuView.getChildAt(i);
                    //noinspection RestrictedApi
                    item.setShiftingMode(false);
                    // set once again checked value, so view will be updated
                    //noinspection RestrictedApi
                    item.setChecked(item.getItemData().isChecked());
                }
            } catch (NoSuchFieldException e) {
                Log.e("BottomNavigationHelper", "Unable to get shift mode field", e);
            } catch (IllegalAccessException e) {
                Log.e("BottomNavigationHelper", "Unable to change value of shift mode", e);
            }
        }
    }
}
