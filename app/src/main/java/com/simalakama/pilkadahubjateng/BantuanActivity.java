package com.simalakama.pilkadahubjateng;

import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListView;

import com.simalakama.pilkadahubjateng.adapter.ListBantuanAdapter;
import com.simalakama.pilkadahubjateng.model.Bantuan;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class BantuanActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bantuan);

        // Set a Toolbar to replace the ActionBar and add Padding.
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setPadding(0,getToolBarHeight(),0,0);


        HashMap<String, List<String>> textDetail = Bantuan.getData();
        final ArrayList<String> textHeaderTitle = new ArrayList<String>(textDetail.keySet());

        ExpandableListView expandableListView = findViewById(R.id.list_bantuan);
        View headerView = getLayoutInflater().inflate(R.layout.header_list_bantuan, expandableListView, false);
        headerView.setClickable(false);
        headerView.setBackgroundColor(getResources().getColor(R.color.colorTransparent));

        expandableListView.setAdapter(new ListBantuanAdapter(this,textHeaderTitle,textDetail));
        expandableListView.addHeaderView(headerView);

        DisplayMetrics metrics = new DisplayMetrics();
        this.getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int width = metrics.widthPixels;
        expandableListView.setIndicatorBounds(width - getDipsFromPixel(50),width - getDipsFromPixel(10));

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private int getToolBarHeight() {
        int height;

        Resources myResources = getResources();
        int idStatusBarHeight = myResources.getIdentifier(
                "status_bar_height", "dimen", "android");
        if (idStatusBarHeight > 0) {
            height = getResources().getDimensionPixelSize(idStatusBarHeight);
            //Toast.makeText(getActivity(), "Status Bar Height = " + height, Toast.LENGTH_LONG).show();
        }else{
            height = 0;
            //Toast.makeText(getActivity(), "Resources NOT found", Toast.LENGTH_LONG).show();
        }

        return height;
    }

    public int getDipsFromPixel(float pixels)
    {
        // Get the screen's density scale
        final float scale = getResources().getDisplayMetrics().density;
        // Convert the dps to pixels, based on density scale
        return (int) (pixels * scale + 0.5f);
    }

}
