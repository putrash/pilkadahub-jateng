# What's PilkadaHub ? 

PilkadaHub adalah aplikasi android untuk menjadi sumber utama untuk membantu pemerintah untuk memantau dan memberikan informasi tentang pemilihan Jateng yang akan diadakan tahun ini. Fitur utama dari aplikasi ini adalah melakukan penghitungan cepat secara real-time oleh admin pada setiap titik pengambilan suara dan orang lain dapat memeriksanya.

# List of Features 
* Melihat quickcount secara realtime
* Melihat informasi mengenai calon gubernur dan calon wakil gubernur
* Melihat berita mengenai pilkada tahu ini
* Forum untuk mengajukan pertanyaan ke salah satu atau masing masing calon gubernur
* Mencari informasi Daftar Pemilih Sementara dan Petugas PPDP
* Pengaduan mengenai Pilkada
* Melihat hasil pilkada

# How to Use ?
 It's easy to use this application, you can use it in just seconds, Trust me.
 
# Library
* Retrofit
* Picasso
and many more
 
